const {
  writeResult,
  readTextFromPath,
} = require('./helpers')

function main() {
  readTextFromPath('./input', { recursive: true }, () => writeResult('./output'))
}

main()

