const fs = require('fs')

let content = ''
let recursiveCount = 1

function writeResult(path) { // Write sum of all numbers to file
  fs.stat(path, function (err) {
    const total = content.split(/\s+/).reduce((total, value) => total + (Number(value) || 0), 0)
    if (err) { // If output folder not found
      fs.mkdir(`${path}`, (err) => { // Create output folder
        if (err) {
          return console.error(err)
        }
        writeText(`${path}/duy-mai-result.txt`, total, () => checkResult('./output'))
      });
    } else {
      writeText(`${path}/duy-mai-result.txt`, total, () => checkResult('./output'))
    }
  })
}

function writeText(path, data, callback) { // Write a text to file with callback
  fs.writeFile(path, data, (err) => {
    if (err) {
      return console.error(err)
    }
    if (callback) {
      callback()
    }
  });
}

function readTextFromPath(path, options = {}, callback) { // Read text from file or folder
  const { recursive = false } = options // recursive option for reading provided path recursively
  fs.stat(path, function (err, stats) {
    if (err) {
      return console.error(err)
    }

    if (!stats.isDirectory()) {
      fs.readFile(path, (err, data) => {
        if (err) {
          return console.error(err)
        }
        content += ` ${data}`
        recursiveCount--
        if (callback && (!recursive || recursiveCount === 0)) {
          callback(data)
        }
      });
    } else {
      recursiveCount--
      fs.readdir(path, function (err, files) {
        if (err) {
          return console.error(err)
        }
        files.map(subDir => {
          recursiveCount++
          readTextFromPath(`${path}/${subDir}`, options, callback) // Recursively read all sub-dir of current directory
        })
      });
    }
  })
}

function checkResult(path) {
  fs.readFile(`${path}/duy-mai-result.txt`, (err, data) => {
    fs.readdir(path, function (err, files) {
      if (err) {
        return console.error(err)
      }
      console.log('\x1b[37m%s\x1b[0m', 'List of files with wrong answer:')
      recursiveCount = 0
      files
        .filter(subDir => subDir !== 'duy-mai-result.txt')
        .map(subDir => {
          recursiveCount++
          readTextFromPath(`${path}/${subDir}`, {}, (fileResult) => {
            if (Number(fileResult) !== Number(data)) {
              console.log('\x1b[31m- %s\x1b[0m', subDir)
            }
          })
        })
    })
  })
}

module.exports = {
  writeResult,
  readTextFromPath,
}
