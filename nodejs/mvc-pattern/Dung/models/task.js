// const task = {
//     id: {
//         type: 'number',
//         required: true
//     },
//     taskName: {
//         type: 'string',
//         required: true,
//         unique: true
//     },
//     isDone: {
//         type: 'string',
//         required: false
//     },
//     owner: {
//         type: 'number',
//         required: true
//     },
// }
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const taskSchema = new Schema({
    taskName: {
        type: String,
        required: true
    },
    isDone: {
        type: Boolean,
        required: true
    },
    owner: {
        type: String,
        require: true
    },
});
const Task = mongoose.model('Task', taskSchema);
module.exports = Task