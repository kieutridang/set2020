const { ping, signIn, signUp, getTask, addTask, removeTask, editTask, users, handleNotFound } = require('./controllers')
const url = require('url')
const routes = {
    '/ping': {
        'GET': { controller: ping }
    },
    '/sign-up': {
        'GET': { controller: signUp }
    },
    '/get-task': {
        'GET': { controller: getTask }
    },
    '/sign-in': {
        'POST': { controller: signIn }
    },
    '/add-task': {
        'POST': { controller: addTask }
    },
    '/remove-task': {
        'POST': { controller: removeTask }
    },
    '/edit-task': {
        'POST': { controller: editTask }
    },
    '/users': {
        'GET': { controller: users }
    }
}
function route(request) {
    const parseUrl = url.parse(request.url, true)
    if (routes[parseUrl.pathname] && routes[parseUrl.pathname][request.method]) {
        const currentRouteData = routes[parseUrl.pathname][request.method]
        console.log(currentRouteData)
        return currentRouteData.controller
    }
    else {
        console.log(handleNotFound)
        return handleNotFound
    }
}
module.exports = { route }