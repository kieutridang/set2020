const fs = require('fs').promises
function FileSystemDataSource(databasePath = '') {
    this.databasePath = databasePath
    this.readCollection = function readCollection(collectionName = '') {
        const collectionPath = `${this.databasePath}/${collectionName.toLowerCase()}.txt`
        return fs.readFile(collectionPath)
            .then(data => {
                const parseData = JSON.parse(data)
                return parseData
            })
            .catch(err => {
                console.error(err)
                return []
            })
    }
    this.updateCollection = function updateCollection(collectionName = '', newData) {
        const collectionPath = `${this.databasePath}/${collectionName.toLowerCase()}.txt`
        return fs.writeFile(collectionPath, JSON.stringify(newData)).catch(() => {
            return fs.mkdir("./database")
                .then(() => {
                    fs.writeFile(this.collectionPath, JSON.stringify(newData))
                })
                .catch(err => {
                    console.error(err)
                })
        })
    }
}
module.exports = FileSystemDataSource