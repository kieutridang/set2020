const http = require('http')
const router = require('./router')
const { DBCollections, fileSystemDataSource } = require('./datasources')
const server = http.createServer((request, response) => {
    const controller = router.route(request)
    controller(request, response)
}).listen(8080, () => {
    console.log('server start')
});
const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/toDoList?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&3t.uriVersion=3', { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    console.log('we are connected!')
});

// const userSchema = new mongoose.Schema({
//     username: {
//         type: String,
//         required: true
//     },
//     password: {
//         type: String,
//         required: true
//     }
// });
// userSchema.methods.speak = function () {
//     const greeting = this.username
//         ? "my name is " + this.username
//         : "i don't have name";
//     console.log(greeting)
// }
// const User = mongoose.model('User', userSchema);
// const duyMai = new User({ username: 'duy Mai', password: 'abc' });
// // console.log(duyMai.username); // 'Silence' 
// duyMai.speak()
// duyMai.save().then(data => console.log(data)).catch(err => console.log(err));
// // User.find({ username: "duy Mai" }).then(data => console.log(data))
// // const Dung = mongoose.model('Dung',userSchema)
// // const Dung1 = new Dung({ username: 'duy Mai' });
// // Dung1.save()