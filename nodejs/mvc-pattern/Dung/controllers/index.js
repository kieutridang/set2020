const url = require('url')
const { DBCollections, fileSystemDataSource } = require('../datasources')
const { insertUser, verifyUser, insertTask, removeOneTask, editOneTask,findTask } = require('./helpers')
const { handleError } = require('../helper')
const jwt = require('jsonwebtoken')
const userRepository = require('../repositories/user.repository')
function ping(request, response) {
    response.end('success')
}
function signUp(request, response) {
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            const user = JSON.parse(chunks.length > 0 ? chunks : '{}')
            insertUser(user)
                .then(() => {
                    handleAuthResponse(response, true)
                })
                .catch(err => {
                    handleError(err, 'controllers/index.js', 'signUp')
                    handleAuthResponse(response, false)
                })
        })
}
function signIn(request, response) {
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            const user = JSON.parse(chunks.toString())
            response.setHeader('Content-Type', 'application/json');
            verifyUser(user).then(foundUser => {
                if (!foundUser) {
                    throw new Error('User not found')
                }
                const token = jwt.sign(
                    { userId: foundUser.id },
                    'RANDOM_TOKEN_SECRET',
                    { expiresIn: '24h' }
                )
                const data = {
                    token
                }
                response.end(JSON.stringify(data));
            }).catch(err => {
                handleError(err, 'controllers/index.js', 'signIn')
                response.statusCode = 404
                response.end('Username or password is not correct.')
            })
        })
}
function getTask(request, response) {
    const chunks = []
    request.on('data', chunk => {
        chunks.push(chunk)
    }).on('end', () => {
        const user = JSON.parse(chunks.toString())
        // chỗ này không biết làm jwt nên là gửi thẳng 1 object {userID: id} =)))
        findTask(userId)
            .then(data => {
                response.end(JSON.stringify(data))
            })
            .catch(err => {
                handleError(err, 'controllers/index.js', 'addTask')
                handleAuthResponse(response, false)
            })
    })

}
function addTask(request, response) {
    const chunks = []
    request.on('data', chunk => {
        chunks.push(chunk)
    }).on('end', () => {
        let task = Buffer.concat(chunks).toString()
        task = JSON.parse(task)
        console.log(task)
        // chỗ này cũng thế không biết xài jwt nên gửi 1 object là {taskname: xyz, userID: id}
        let user = task.userID
        insertTask(task, user)
        response.end('add success')
    })
}
function removeTask(request, response) {
    const chunks = []
    request.on('data', chunk => {
        chunks.push(chunk)
    }).on('end', () => {
        let task = Buffer.concat(chunks).toString()
        task = JSON.parse(task)
        let user = task.userID
        removeOneTask(task, user)
        response.end('remove success')
    })
}
function editTask(request, response) {
    const chunks = []
    request.on('data', chunk => {
        chunks.push(chunk)
    }).on('end', () => {
        let task = Buffer.concat(chunks).toString()
        task = JSON.parse(task)
        console.log(task)
        // let user = task.userID
        editOneTask(task)
        response.end('edit success')
    })
}
function handleNotFound(req, res) {
    const parseUrl = url.parse(req.url, true)
    res.statusCode = 404
    res.end(`Route ${parseUrl.pathname} not found.`)
}
function users(req, res) {
    userRepository.find()
        .then(data => {
            res.end(JSON.stringify(data))
        })
        .catch(err => {
            console.log(err)
        })
}
module.exports = {
    ping,
    signIn,
    signUp,
    getTask,
    addTask,
    removeTask,
    editTask,
    users,
    handleNotFound
}