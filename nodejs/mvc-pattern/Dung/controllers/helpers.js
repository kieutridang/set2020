const crypto = require('crypto')
const { DBCollections, fileSystemDataSource } = require('../datasources')
const { userRepository,taskRepository } = require('../repositories')
const {User, Task} = require('../models')
// const Repository = require('../repositories')
function insertUser(user) {
    const password = user.password ? hashPassword(user.password) : undefined
    // const newUser = {
    //     username: user.username,
    //     password
    // }
    // return userRepository.createOne(newUser)
    const newUser = new User({
        username: user.username,
        password
    })
    return newUser.save()
}
function hashPassword(password) {
    const hash = crypto.createHash('sha256')
    return hash.update(password).digest('hex')
}
function verifyUser(checkingUser) {
    return User.findOne({
        username: checkingUser.username,
        password: hashPassword(checkingUser.password),
      });
}
function insertTask(task) {
    const newTask = new Task({
        taskName: task.taskName,
        isDone: "false",
        owner: task.owner
    })
    return newTask.save()
}
function removeOneTask(task, user) {
    // fileSystemDataSource.readCollection(DBCollections.TASK).then(data => {
    //     console.log(data)
    //     let allTask = data || []
    //     allTask = allTask.filter(item => item.id !== task.id)
    //     console.log(allTask)
    //     return allTask
    // }).then(updatedTasks => {
    //     fileSystemDataSource.updateCollection(DBCollections.TASK, updatedTasks)
    // }).catch(err => console.error(err))
    Task.remove({_id:task.id})
}
function editOneTask(task) {
    // fileSystemDataSource.readCollection(DBCollections.TASK).then(data => {
    //     console.log(data)
    //     let allTask = data || []
    //     allTask.forEach(item => {
    //         if (item.id === task.id && item.ownerID === user) {
    //             item.taskname = task.taskname
    //         }
    //         return allTask
    //     })
    //     console.log(allTask)
    //     return allTask
    // }).then(updatedTasks => {
    //     fileSystemDataSource.updateCollection(DBCollections.TASK, updatedTasks)
    // }).catch(err => console.error(err))
    Task.findByIdAndUpdate(task.id,{taskName:task.taskName})
}
function findTask(id) {
    return Task.find({owner:id})
}
module.exports = {
    insertUser,
    verifyUser,
    insertTask,
    removeOneTask,
    editOneTask,
    findTask
}