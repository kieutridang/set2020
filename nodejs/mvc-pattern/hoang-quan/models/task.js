// const task = {
//     id: {
//         type: 'string',
//         required: true
//     },
//     taskName: {
//         type: 'string',
//         required: true,
//     },
//     isDone: {
//         type: 'boolean',
//         required: true
//     },
//     owner: {
//         type: 'string',
//         required: true
//     }
// }
const mongoose = require('mongoose');
const taskSchema = new mongoose.Schema({
    taskName: {
        type: String,
        required: true
    },
    isDone: {
        type: Boolean,
        required: true
    },
    owner: {
        type: String,
        require: true
    },
})
const Task = mongoose.model('Task', taskSchema)
module.exports = Task