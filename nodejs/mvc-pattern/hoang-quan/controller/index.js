const url = require('url')
const { DBCollections, fileSystemDataSource } = require('../datasources')
const { verifyUserSignIn, verifyUserSignUp } = require('./helper.js')
const { handleError } = require('../helpers')
const { User, Task } = require('../models')
const userRepository = require('../repositories/user.repository')
const taskRepository = require('../repositories/task.repository')

function signIn(request, response) {
    console.log('go sign in')
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            User.find()
                .then(data => {
                    let allUsers = data
                    console.log(bodyData)
                    let flag = verifyUserSignIn(allUsers, bodyData)
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify(flag));
                    response.end()
                })
                .catch(err => {
                    handleError(err, 'controller/index.js', 'signIn')
                    return []
                })
                // userRepository.find()
                //     .then(data => {
                //         let allUsers = data
                //         console.log(bodyData)
                //         let flag = verifyUserSignIn(allUsers, bodyData)
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify(flag));
                //         response.end()
                //     })
                //     .catch(err => {
                //         handleError(err, 'controller/index.js', 'signIn')
                //         return []
                //     })
        })
}

function signUp(request, response) {
    console.log('go sign up')
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            const newUser = new User({
                username: bodyData.username,
                password: bodyData.password
            })
            return newUser.save()
                .then(data => {
                    User.find()
                        .then(data => {
                            console.log(data[data.length - 1]._id)
                            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                            response.write(JSON.stringify(data[data.length - 1]._id));
                            response.end()
                        })
                })
                .catch(err => {
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify('deny'));
                    response.end()
                    handleError(err, 'controller/index.js', 'signup')
                    return []
                })
        })
}

function getTask(request, response) {
    console.log('go get task')
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            Task.find({ owner: bodyData.currentUser })
                .then(data => {
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify(data))
                    response.end()
                })
                .catch(err => {
                    handleError(err, 'controller/index.js', 'getUsers')
                    return []
                })
        })
}

function addTask(request, response) {
    console.log('go add task')
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            const newTask = new Task({
                taskName: bodyData.taskName,
                isDone: bodyData.isDone,
                owner: bodyData.owner
            })
            newTask.save()
                .then(data => {
                    Task.find()
                        .then(data => {
                            const returnData = {
                                status: 'success',
                                id: data[data.length - 1]._id,
                            }
                            console.log(data[data.length - 1]._id)
                            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                            response.write(JSON.stringify(returnData));
                            response.end()
                        })
                        .catch(err => {
                            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                            response.write(JSON.stringify('deny'));
                            response.end()
                            handleError(err, 'controller/index.js', 'signup')
                            return []
                        })
                })
                // taskRepository.createOne(newTask)
                //     .then(data => {
                //         console.log('success', data)
                //         const returnData = {
                //             status: 'success',
                //             id: data[data.length - 1].id,
                //         }
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify(returnData))
                //         response.end()
                //     })
                //     .catch(err => {
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify('deny'));
                //         response.end()
                //         handleError(err, 'controller/index.js', 'signup')
                //         return []
                //     })

        })
}

function deleteTask(request, response) {
    console.log('go delete task')
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            const data = {
                id: bodyData.currentTask,
            }
            console.log(data.id)
            Task.remove({ _id: data.id })
                .then(data => {
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify('success'))
                    response.end()
                })
                .catch(err => {
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify('deny'));
                    response.end()
                    handleError(err, 'controller/index.js', 'signup')
                    return []
                })
                // taskRepository.deleteOne(data)
                //     .then(data => {
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify(data))
                //         response.end()
                //     })
                //     .catch(err => {
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify('deny'));
                //         response.end()
                //         handleError(err, 'controller/index.js', 'signup')
                //         return []
                //     })

        })
}

function editTask(request, response) {
    console.log('go edit task')
    const chunks = []
    request
        .on('data', (chunk) => {
            chunks.push(chunk)
        })
        .on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            const data = {
                _id: bodyData.id,
                taskName: bodyData.taskName,
                isDone: bodyData.isDone,
                owner: bodyData.owner
            }
            console.log(bodyData)
            Task.findByIdAndUpdate(bodyData.id, { taskName: bodyData.taskName })
                .then(data => {
                    console.log(data)
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify(data))
                    response.end()

                })
                .catch(err => {
                    response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                    response.write(JSON.stringify('deny'));
                    response.end()
                    handleError(err, 'controller/index.js', 'signup')
                    return []
                })
                // taskRepository.updateOne(data)
                //     .then(data => {
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify(data))
                //         response.end()
                //     })
                //     .catch(err => {
                //         response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                //         response.write(JSON.stringify('deny'));
                //         response.end()
                //         handleError(err, 'controller/index.js', 'signup')
                //         return []
                //     })
        })
}

function getUsers(request, response) {

    userRepository.find()
        .then(data => {
            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
            response.end(JSON.stringify(data))
        })
        .catch(err => {
            handleError(err, 'controller/index.js', 'getUsers')
            return []
        })

}

function handleNotFound(request, response) {
    const parseUrl = url.parse(request.url, true)
    response.statusCode = 404
    response.end(`Route ${parseUrl.pathname} not found.`)
}
module.exports = {
    signIn,
    signUp,
    getTask,
    addTask,
    deleteTask,
    editTask,
    getUsers,
    handleNotFound
}