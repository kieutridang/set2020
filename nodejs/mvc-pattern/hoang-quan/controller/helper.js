function verifyUserSignIn(allUsers, bodyData) {
    for (let i = 0; i < allUsers.length; i++) {
        if (allUsers[i].username == bodyData.username && allUsers[i].password == bodyData.password) {
            console.log('user sign in id - ', allUsers[i]._id)
            return allUsers[i]._id
        }
    }
    return 'deny'
}

function verifyUserSignUp(allUsers, bodyData) {
    let flag = true
    for (let i = 0; i < allUsers.length; i++) {
        if (allUsers[i].username == bodyData.username) {
            flag = false
            break;
        }
    }
    return flag
}

module.exports = { verifyUserSignIn, verifyUserSignUp }