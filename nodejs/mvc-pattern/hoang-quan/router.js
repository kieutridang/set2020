const url = require('url')
const { signIn, signUp, handleNotFound, getTask, addTask, deleteTask, editTask, getUsers } = require('./controller')

const routes = {
    '/sign-in': {
        'POST': {
            controller: signIn
        }
    },
    '/sign-up': {
        'POST': {
            controller: signUp
        }
    },
    '/get-task': {
        'POST': {
            controller: getTask
        }
    },
    '/add-task': {
        'POST': {
            controller: addTask
        }
    },
    '/delete-task': {
        'POST': {
            controller: deleteTask
        }
    },
    '/edit-task': {
        'POST': {
            controller: editTask
        }
    },
    '/users': {
        'POST': {
            controller: getUsers
        }
    }
}

function route(request) {
    const parseUrl = url.parse(request.url, true)
    if (routes[parseUrl.pathname] && routes[parseUrl.pathname][request.method]) {
        return routes[parseUrl.pathname][request.method].controller
    }
    return handleNotFound
}
module.exports = { route }