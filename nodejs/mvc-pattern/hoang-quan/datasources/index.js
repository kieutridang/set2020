const FileSystemDataSource = require('./file-system.datasource')

const database = './database'

const DBCollections = {
    USER: 'users',
    TASK: 'tasks'
}
const fileSystemDataSource = new FileSystemDataSource(database)

module.exports = { DBCollections, fileSystemDataSource }