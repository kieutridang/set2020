const { DBCollections } = require('../datasources')
const { userModel } = require('../models')
const Repository = require('./base.repository')

const userRepository = new Repository(DBCollections.USER, userModel)
module.exports = userRepository