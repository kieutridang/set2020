const { DBCollections, fileSystemDataSource } = require('../datasources')
const { validateEntityFields, validateEntityUniqueness, validateEntityFieldsWithId } = require('./helpers')
const { handleError } = require('../helpers')

function Repository(name, schema) {
    this.schema = schema
    this.name = name
    this.find = function find() {
        return fileSystemDataSource.readCollection(this.name)
            .then(data => {
                return data
            })
            .catch(err => {
                handleError(err, ' repositories/base.repository.js', 'find')
                return []
            })
    }
    this.createOne = function createOne(newItem) {
        return new Promise((resolve, reject) => {
            let validationError = validateEntityFields(this.schema, newItem)
            if (validationError) {
                reject(validationError)
            } else {
                resolve(this.find().then(existingItems => {
                    validationError = validateEntityUniqueness(this.schema, newItem, existingItems)
                    if (validationError) {
                        throw new Error(validationError)
                    }
                    let tempId = Number(existingItems[existingItems.length - 1].id) + 1
                    existingItems.push({
                        id: tempId.toString(),
                        ...newItem
                    })
                    return fileSystemDataSource.updateCollection(this.name, existingItems)
                        .then(() => {
                            return existingItems
                        })
                        .catch(err => {
                            handleError(err, 'repositories/base.repository.js', 'createOne')
                        })
                }))
            }
        })
    }
    this.deleteOne = function deleteOne(newItem) {
        return new Promise((resolve, reject) => {
            let validationError = validateEntityFieldsWithId(this.schema, newItem)
            if (validationError) {
                reject(validationError)
            } else {
                resolve(this.find().then(existingItems => {
                    let newAllTask = existingItems.filter(item => item.id != newItem.id)
                    return fileSystemDataSource.updateCollection(this.name, newAllTask)
                        .then(() => {
                            return 'success'
                        })
                        .catch(err => {
                            handleError(err, 'repositories/base.repository.js', 'deleteOne')
                        })
                }))
            }
        })
    }
    this.updateOne = function updateOne(newItem) {
        return new Promise((resolve, reject) => {
            let validationError = validateEntityFieldsWithId(this.schema, newItem)
            if (validationError) {
                reject(validationError)
            } else {
                resolve(this.find().then(existingItems => {
                    for (let i = 0; i < existingItems.length; i++) {
                        if (existingItems[i].id == newItem.id) {
                            existingItems[i].taskName = newItem.taskName
                        }
                    }
                    return fileSystemDataSource.updateCollection(this.name, existingItems)
                        .then(() => {
                            return 'success'
                        })
                        .catch(err => {
                            handleError(err, 'repositories/base.repository.js', 'deleteOne')
                        })
                }))
            }
        })
    }
}

module.exports = Repository