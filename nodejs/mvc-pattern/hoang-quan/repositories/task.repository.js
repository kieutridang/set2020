const { DBCollections } = require('../datasources')
const { taskModel } = require('../models')
const Repository = require('./base.repository')

const taskRepository = new Repository(DBCollections.TASK, taskModel)
module.exports = taskRepository