function handleSignin() {
  let xhttp = new XMLHttpRequest()
  const usernameElement = document.getElementById('username')
  const passwordElement = document.getElementById('password')
  const data = {
    username: usernameElement.value,
    password: passwordElement.value
  }
  xhttp.open('POST', 'http://localhost:8080/sign-in', true)
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById('text').innerHTML = this.responseText
    }
  }
  xhttp.send(JSON.stringify(data))
}

let loginButton = document.getElementById('sign-in')
loginButton.addEventListener('click', handleSignin)