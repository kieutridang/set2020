const fs = require('fs').promises
const crypto = require('crypto')
const http = require('http')

const server = http.createServer((request, response) => {
  const chunks = []
  responseStatus = {}
  let bodyData
  request.on('data', chunk => {
    chunks.push(chunk)
  }).on('end', () => {
    bodyData = Buffer.concat(chunks).toString()
    if (bodyData) {
      bodyData = JSON.parse(bodyData)
      bodyData.password = crypto.createHash('sha256').update(bodyData.password).digest('hex')
    }
  })

  fs.readFile('./users.txt')
    .then(data => {
      data = JSON.parse(data)
      if (request.url == '/sign-up' && request.method == 'POST') {
        if (data.every(item => item.username != bodyData.username)) {
          responseStatus.status = 'success'
          data.push(bodyData)
        } else {
          responseStatus.status = 'fail'
        }
        fs.writeFile('./users.txt', JSON.stringify(data))
      } else if (request.url == '/sign-in' && request.method == 'POST') {
        let matchAccount = data.find(item => item.username == bodyData.username && item.password == bodyData.password)
        if (matchAccount) {
          responseStatus.status = 'success'
        } else {
          responseStatus.status = 'fail'
        }
      } else {
        responseStatus.status = 'fail'
      }
    })
    .then(() => {
      response.writeHead(200, {'Access-Control-Allow-Origin':'*'})
      response.write(JSON.stringify(responseStatus))
      response.end()
    })
    .catch(err => console.log(err))
}).listen(8080, () => {
  console.log('server started')
})
