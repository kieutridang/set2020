function Pawn(color = "black", moveHistory = []) {
  ChessPiece.call(this, color, moveHistory);
  Pawn.prototype = Object.create(ChessPiece.prototype);
  Pawn.prototype.constructor = Pawn;

  this.findAvailableMove = function findAvailableMove(toggle) {
    const currentCellCoordination = this.getCurrentPosition()
    const availableMoveCoordinations = [
      {
        x: currentCellCoordination.x,
        y: color == 'black' ? currentCellCoordination.y - 1 : currentCellCoordination.y + 1
      },
      {
      x: currentCellCoordination.x,
      y: color == 'black' ? currentCellCoordination.y - 2 : currentCellCoordination.y + 2
      }
    ];
    for (let index = 0; index < availableMoveCoordinations.length; index++) {
      this.renderAvailableCell(availableMoveCoordinations[index], toggle)
    }
  }

  this.block.src =
    color == "black"
      ? "./assets/img/pawn_black.png"
      : "./assets/img/pawn_white.png";
  return this.render();

}
