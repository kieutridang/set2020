function Bishop(color = "black", moveHistory = []) {
  ChessPiece.call(this, color, moveHistory);
  Bishop.prototype = Object.create(ChessPiece.prototype);
  Bishop.prototype.constructor = Bishop;

  this.block.src =
    color == "black"
      ? "./assets/img/bishop_black.png"
      : "./assets/img/bishop_white.png";
  return this.render();
}
