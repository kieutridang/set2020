function Brook(color = "black", moveHistory = []) {
  ChessPiece.call(this, color, moveHistory);
  Brook.prototype = Object.create(ChessPiece.prototype);
  Brook.prototype.constructor = Brook;

  this.findAvailableMove = function findAvailableMove(toggle) {
    let availableMoveCoordinations = []
    const verticalUpperCells = getVerticalCells('up', this.block.parentElement);
    const horizontalBelowCells = getVerticalCells('down', this.block.parentElement);
    availableMoveCoordinations = availableMoveCoordinations.concat(horizontalBelowCells);
    availableMoveCoordinations = availableMoveCoordinations.concat(verticalUpperCells);
    for (let index = 0; index < availableMoveCoordinations.length; index++) {
      this.renderAvailableCell(availableMoveCoordinations[index], toggle)
    }
  }

  this.block.src =
    color == "black"
      ? "./assets/img/rook_black.png"
      : "./assets/img/rook_white.png";
  return this.render();
}