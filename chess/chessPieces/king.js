function King(color = "black", moveHistory = []) {
  ChessPiece.call(this, color, moveHistory);
  King.prototype = Object.create(ChessPiece.prototype);
  King.prototype.constructor = King;

  this.block.src =
    color == "black"
      ? "./assets/img/king_black.png"
      : "./assets/img/king_white.png";
  return this.render();
}