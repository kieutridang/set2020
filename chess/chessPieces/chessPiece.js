function ChessPiece(color = "black", moveHistory = []) {
  this.color = color;
  this.checkMate = false;
  this.moveHistory = moveHistory;
  this.isSelected = false;
  this.block = document.createElement("img");
  this.findAvailableMove = function findAvailableMove() {};
  this.move = function (x, y) {};
  this.render = function render() {
    const blockStyle = this.block.style;
    blockStyle.position = "absolute";
    blockStyle.margin = "auto";
    blockStyle.top = "0";
    blockStyle.left = "0";
    blockStyle.right = "0";
    blockStyle.bottom = "0";
    blockStyle.cursor = "pointer";
    this.block.attributes.data = this;
    return this.block;
  };

  this.renderAvailableCell = function renderAvailableCell(availableMoveCoordination, toggle) {
    const chessBoard = document.getElementById('chess-board')
    const chessBoardData = chessBoard.attributes.data
    const cell = chessBoardData.getCell(availableMoveCoordination.x, availableMoveCoordination.y)
    const cellData = cell.attributes.data
    if (!cell.hasChildNodes()) {
      cellData.toggleRenderAvailableCell(toggle)
    }
  }

  this.toggleSelectRender = function selectRender() {
    const blockStyle = this.block.style;
    if (this.isSelected) {
      blockStyle.marginTop = "auto";
      blockStyle.opacity = "1";
      this.findAvailableMove(false);
    } else {
      blockStyle.marginTop = "5px";
      blockStyle.opacity = "0.7";
      this.findAvailableMove(true);
    }
    const currentCellCoordination = {
      x: this.block.parentNode.attributes.data.x,
      y: this.block.parentNode.attributes.data.y,
    };
    document.dispatchEvent(
      new CustomEvent("deselectAll", {
        detail: { byPassCoordination: currentCellCoordination },
      })
    );
    this.isSelected = !this.isSelected;
  };
  this.block.addEventListener("click", this.toggleSelectRender.bind(this));

  this.deselectRender = function deselectRender() {
    const blockStyle = this.block.style;
    if (this.isSelected) {
      blockStyle.marginTop = "auto";
      blockStyle.opacity = "1";
      this.findAvailableMove(false);
      this.isSelected = !this.isSelected;
    }
  };

  this.getCurrentPosition = function getCurrentPosition() {
    const parentData =  this.block.parentElement.attributes.data
    const position = {
      x: parentData.x,
      y: parentData.y
    }
    return position
  }
}