function Knight(color = "black", moveHistory = []) {
  ChessPiece.call(this, color, moveHistory);
  Knight.prototype = Object.create(ChessPiece.prototype);
  Knight.prototype.constructor = Knight;

  this.block.src =
    color == "black"
      ? "./assets/img/knight_black.png"
      : "./assets/img/knight_white.png";
  return this.render();
}