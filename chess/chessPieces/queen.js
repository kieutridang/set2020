function Queen(color = "black", moveHistory = []) {
  ChessPiece.call(this, color, moveHistory);
  Queen.prototype = Object.create(ChessPiece.prototype);
  Queen.prototype.constructor = Queen;

  this.block.src =
    color == "black"
      ? "./assets/img/queen_black.png"
      : "./assets/img/queen_white.png";
  return this.render();
}