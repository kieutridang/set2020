const chessBoard = new ChessBoard('chess-board')

// White
const whiteKing = new King('white')
const whiteBrook1 = new Brook('white')
const whiteKnight1 = new Knight('white')
const whiteBishop1 = new Bishop('white')
const whiteQueen = new Queen('white')
const whiteBishop2 = new Bishop('white')
const whiteKnight2 = new Knight('white')
const whiteBrook2 = new Brook('white')

const whitePawn1 = new Pawn('white')
const whitePawn2 = new Pawn('white')
const whitePawn3 = new Pawn('white')
const whitePawn4 = new Pawn('white')
const whitePawn5 = new Pawn('white')
const whitePawn6 = new Pawn('white')
const whitePawn7 = new Pawn('white')
const whitePawn8 = new Pawn('white')

// Black
const blackKing = new King('black')
const blackBrook1 = new Brook('black')
const blackKnight1 = new Knight('black')
const blackBishop1 = new Bishop('black')
const blackQueen = new Queen('black')
const blackBishop2 = new Bishop('black')
const blackKnight2 = new Knight('black')
const blackBrook2 = new Brook('black')
const blackPawn1 = new Pawn('black')
const blackPawn2 = new Pawn('black')
const blackPawn3 = new Pawn('black')
const blackPawn4 = new Pawn('black')
const blackPawn5 = new Pawn('black')
const blackPawn6 = new Pawn('black')
const blackPawn7 = new Pawn('black')
const blackPawn8 = new Pawn('black')

function init() {
  // White
  let cell = chessBoard.getCell(0, 0)
  cell.appendChild(whiteBrook1)

  cell = chessBoard.getCell(1, 0)
  cell.appendChild(whiteKnight1)

  cell = chessBoard.getCell(2, 0)
  cell.appendChild(whiteBishop1)

  cell = chessBoard.getCell(3, 0)
  cell.appendChild(whiteKing)

  cell = chessBoard.getCell(4, 0)
  cell.appendChild(whiteQueen)

  cell = chessBoard.getCell(5, 0)
  cell.appendChild(whiteBishop2)

  cell = chessBoard.getCell(6, 0)
  cell.appendChild(whiteKnight2)

  cell = chessBoard.getCell(7, 0)
  cell.appendChild(whiteBrook2)

  cell = chessBoard.getCell(0, 1)
  cell.appendChild(whitePawn8)

  cell = chessBoard.getCell(1, 1)
  cell.appendChild(whitePawn1)

  cell = chessBoard.getCell(2, 1)
  cell.appendChild(whitePawn2)

  cell = chessBoard.getCell(3, 1)
  cell.appendChild(whitePawn3)

  cell = chessBoard.getCell(4, 1)
  cell.appendChild(whitePawn4)

  cell = chessBoard.getCell(5, 1)
  cell.appendChild(whitePawn5)

  cell = chessBoard.getCell(6, 1)
  cell.appendChild(whitePawn6)

  cell = chessBoard.getCell(7, 1)
  cell.appendChild(whitePawn7)

  // Black
  cell = chessBoard.getCell(0, 7)
  cell.appendChild(blackKing)

  cell = chessBoard.getCell(1, 7)
  cell.appendChild(blackBrook1)

  cell = chessBoard.getCell(2, 7)
  cell.appendChild(blackKnight1)

  cell = chessBoard.getCell(3, 7)
  cell.appendChild(blackBishop1)

  cell = chessBoard.getCell(4, 7)
  cell.appendChild(blackQueen)

  cell = chessBoard.getCell(5, 7)
  cell.appendChild(blackBishop2)

  cell = chessBoard.getCell(6, 7)
  cell.appendChild(blackKnight2)

  cell = chessBoard.getCell(7, 7)
  cell.appendChild(blackBrook2)

  cell = chessBoard.getCell(0, 6)
  cell.appendChild(blackPawn1)

  cell = chessBoard.getCell(1, 6)
  cell.appendChild(blackPawn2)

  cell = chessBoard.getCell(2, 6)
  cell.appendChild(blackPawn3)

  cell = chessBoard.getCell(3, 6)
  cell.appendChild(blackPawn4)

  cell = chessBoard.getCell(4, 6)
  cell.appendChild(blackPawn5)

  cell = chessBoard.getCell(5, 6)
  cell.appendChild(blackPawn6)

  cell = chessBoard.getCell(6, 6)
  cell.appendChild(blackPawn7)

  cell = chessBoard.getCell(7, 6)
  cell.appendChild(blackPawn8)
}

init()