function Cell(x = 0, y = 0, color = "gray") {
  this.x = x;
  this.y = y;
  this.color = color;
  this.block = document.createElement("div");

  this.render = function render() {
    this.block.style.display = "inline-block";
    this.block.style.width = "80px";
    this.block.style.height = "80px";
    this.block.style.border = '1px solid black';
    this.block.style.position = "relative";
    this.block.style.backgroundColor = this.color;
    this.block.attributes.data = this
    return this.block;
  };

  this.toggleRenderAvailableCell = function toggleRenderAvailableCell(toggle) {
    if (toggle) {
      this.block.style.backgroundColor = 'green'
    } else {
      this.block.style.backgroundColor = this.color
    }
  }
  return this.render();
}

function Row(line) {
  this.line = line;

  this.block = document.createElement("div");
  this.render = function render() {
    this.block.style.display = "block";
    this.block.style.height = "80px";
    this.block.attributes.data = this
    return this.block;
  };
  return this.render();
}

function ChessBoard(id) {
  this.rowsNumber = 8;
  this.columnsNumber = 8;
  this.cells = [];
  this.rows = [];
  this.block = document.getElementById(id);
  for (let rowNumber = 0; rowNumber < this.rowsNumber; rowNumber++) {
    const row = new Row(rowNumber);
    this.rows.push(row);
    for (let columnNumber = 0; columnNumber < this.columnsNumber; columnNumber++) {
      let color
      if (rowNumber % 2 == 0) {
        color = columnNumber % 2 == 0 ? 'white': 'gray';
      } else {
        color = columnNumber % 2 == 0 ? 'gray': 'white';
      }
      const cell = new Cell(columnNumber, rowNumber, color);
      row.appendChild(cell);
      this.cells.push(cell);
    }
  }

  this.getCell = function getCell(x, y) {
    for(let index = 0; index < this.cells.length; index++) {
      const cellData = this.cells[index].attributes.data
      if (cellData.x === x && cellData.y === y) {
        return this.cells[index]
      }
    }
  }

  this.render = function render() {
    this.rows.forEach((row) => {
      this.block.appendChild(row);
    });
    this.block.attributes.data = this
    return this.block;
  };
  this.render();
}

document.addEventListener('deselectAll', function deselectAll(event) {
  const detail = event.detail
  const byPassCoordination = detail.byPassCoordination
  const chessBoard = document.getElementById('chess-board')
  const chessBoardData = chessBoard.attributes.data
  for (let index = 0; index < chessBoardData.cells.length; index++) {
    const cell = chessBoardData.cells[index]
    const cellData = cell.attributes.data
    const canByPass = cellData.x == byPassCoordination.x && cellData.y == byPassCoordination.y
    if (cell.hasChildNodes() && !canByPass) {
      const chessPiece = cell.firstElementChild.attributes.data
      chessPiece.deselectRender()
    }
  }
})