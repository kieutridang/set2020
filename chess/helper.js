function getVerticalCells(direction, rootCell, to) {
  const currentY = rootCell.attributes.data.y;
  const currentX = rootCell.attributes.data.x;
  const verticalCellCoordinations = []
  if (direction === 'up') {
    const numberOfUpperCells = 7 - currentY - 1
    for(let index = 0; index < numberOfUpperCells; index++) {
      const coordination = {
        x: currentX,
        y: currentY + index
      }
      verticalCellCoordinations.push(coordination)
    }
  } else {
    const numberOfBelowCells =  currentY + 1
    for(let index = 0; index < numberOfBelowCells; index++) {
      const coordination = {
        x: currentX,
        y: currentY - index
      }
      verticalCellCoordinations.push(coordination)
    }
  }
  return verticalCellCoordinations
}