console.log("Hello world!")
var result = 0
for (let i = 0; i <= 1000; i++) {
    result = result + i;
}
console.log(result)


for (let i = 2; i <= 10; i++) {
    var flag = 1;
    for (let j = 2; j <= i - 1; j++) {
        if ( i % j == 0) {
            flag = 0
            break
        }
    }
    if (flag == 1) {
        console.log(i)
    }
    else if (flag == 0) {
        flag = 1;
    }
}

var a = [500, 200, 100, 50, 20, 10, 5, 2, 1]
var b = [0, 0, 0, 0, 0, 0, 0, 0, 0]
var x = 5432
var i = 0
while (x != 0) {
    if (x >= a[i]) {
        x= x - a[i]
        b[i] = b[i] + 1
        console.log(a[i])
    }
    else {
        i = i + 1
    }
}
var A = {
    x : 0,
    y : -1,
    z : 2
}
var B = {
    x : -1,
    y : 2,
    z : -3
}
var C = {
    x : 0,
    y : 0,
    z : -2
}
var P = {
    x : -1/3,
    y : 1/3,
    z : -1
}
var AB = [B.x - A.x, B.y-A.y, B.z-A.z]
var AC = [C.x - A.x, C.y-A.y, C.z-A.z]


var ABAC = []
ABAC[0]= (AB[1] * AC[2]) - (AB[2] * AC[1])
ABAC[1]= - (AB[0]) * AC[2] - (AB[2] * AC[0])
ABAC[2]= (AB[0] * AC[1]) - (AB[1] * AC[0])

var d = - (ABAC[0] * A.x + ABAC[1] * A.y + ABAC[2] * A.z)

var check = - (ABAC[0] * P.x + ABAC[1] * P.y + ABAC[2] * P.z)
console.log(d)
console.log(check)
function length(x1, x2, y1, y2, z1, z2) {
    let result 
    result = Math.sqrt(((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2)) + ((z1 - z2) * (z1 - z2)))
    return result
}
function area(a, b, c) {
    let halfPerimeter = (a + b + c) / 2
    let surface = Math.sqrt(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c))
    return surface
}
if (Math.abs(d - check) < 0.000001) {
    console.log("DIEM X THUOC MAT PHANG")
    let lengthAB = length(A.x, B.x, A.y, B.y, A.z, B.z)
    let lengthAC = length(A.x, C.x, A.y, C.y, A.z, C.z)
    let lengthBC = length(B.x, C.x, B.y, C.y, B.z, C.z)
    let lengthPA = length(P.x, A.x, P.y, A.y, P.z, A.z)
    let lengthPB = length(P.x, B.x, P.y, B.y, P.z, B.z)
    let lengthPC = length(P.x, C.x, P.y, C.y, P.z, C.z)

    squareABC = (area(lengthAB, lengthAC, lengthBC))
    squareP = (area(lengthAB, lengthPA, lengthPB) + area(lengthAC, lengthPA, lengthPC) + area(lengthBC, lengthPB, lengthPC))
    if (Math.abs(squareABC - squareP) < 0.000001) {
        console.log("DIEM X NAM TRONG TAM GIAC")
    }
    else {
        console.log("DIEM X KHONG NAM TRONG TAM GIAC")
    }
}
else {
    console.log("DIEM X KHONG THUOC MAT PHANG")
    let distance = (Math.abs(ABAC[0] * P.x + ABAC[1] * P.y + ABAC[2] * P.z + d)) / (Math.sqrt(ABAC[0] * ABAC[0] + ABAC[1] * ABAC[1] + ABAC[2] * ABAC[2]))
    console.log(distance)
}


// lam lai bang declarative function

function point (x, y, z) {
    this.x = x 
    this.y = y
    this.z = z 
}
function flat (a, b, c, d) {
    this.a = a 
    this.b = b
    this.c = c 
    this.d = d 
    this.findFlat = findFlat
    this.checkPointBelongToFlat = checkPointBelongToFlat
    this.distanceFromPointToFlat = distanceFromPointToFlat
}
function vector (a, b, c) {
    this.a = a
    this.b = b 
    this.c = c
    this.findVector = findVector
    this.multipleVector = multipleVector
}
function findVector(A, B) {
    this.a = B.x - A.x
    this.b = B.y - A.y
    this.c = B.z - A.z 
}
function multipleVector (AB, AC) {
    this.a = (AB.b * AC.c) - (AB.c * AC.b)
    this.b = - (AB.a * AC.c) - (AB.c * AC.a)
    this.c = (AB.a * AC.b) - (AB.b * AC.a)
}
function findFlat (ABAC, A) {
    this.a = ABAC.a 
    this.b = ABAC.b
    this.c = ABAC.c 
    this.d = - (ABAC.a * A.x + ABAC.b * A.y + ABAC.c * A.z)
}
function checkPointBelongToFlat (P) {
    if (Math.abs(this.a * P.x + this.b * P.y + this.c * P.z + this.d) < 0.00001) {
        return 1
    }
    else {
        return 0
    }
}
function distanceFromPointToFlat(P) {
    return ((Math.abs(this.a * P.x + this.b * P.y + this.c * P.z + d)) / (Math.sqrt(this.a * this.a + this.b * this.b + this.c * this.c)))
}
function findEdge(A, B) {
    return (Math.sqrt(((A.x - B.x) * (A.x - B.x)) + ((A.y - B.y) * (A.y - B.y)) + ((A.z - B.z) * (A.z - B.z))))
}
function findTriangleArea(A, B, C) {
    let edge1 = findEdge(A, B)
    let edge2 = findEdge(A, C)
    let edge3 = findEdge(B, C)
    let halfPerimeter = (edge1 + edge2 + edge3) / 2
    return (Math.sqrt(halfPerimeter * (halfPerimeter - edge1) * (halfPerimeter - edge2) * (halfPerimeter - edge3)))
}
var A = new point(0, -1, 2)
var B = new point(-1, 2 ,-3)
var C = new point(0, 0, -2)
var P = new point(-1/3, 1/3, -1)

var AB = new vector()
var AC = new vector()
AB.findVector(A, B)
AC.findVector(A, C)
var ABAC = new vector()
ABAC.multipleVector(AB, AC)
console.log(ABAC)
var flatABC = new flat()
flatABC.findFlat(ABAC, A) 
console.log(flatABC) 
var flag = flatABC.checkPointBelongToFlat(P)
console.log(flag)
if (flag == 1) {
    console.log("Diem X thuoc mat phang")
    surfaceABC = findTriangleArea(A, B, C)
    surfaceCheck = findTriangleArea(A, B, P) + findTriangleArea(A, C, P) + findTriangleArea(C, B, P)
    if (Math.abs(surfaceABC - surfaceCheck) < 0.00001) {
        console.log("Diem X nam trong tam giac")
    }
    else {
        console.log("Diem X khong nam trong tam giac")
    }
}
else {
    console.log("Diem X khong thuoc mat phang")
    console.log(flatABC.distanceFromPointToFlat(P))
}
