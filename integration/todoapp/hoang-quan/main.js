var chosenTask
if (localStorage.getItem('currentUser') == undefined)
    location.href = "./login.html"
    // var allTask = JSON.parse(localStorage.task)
    // var activeTask = allTask.filter(item => item.owner == JSON.parse(localStorage.currentUser))
var toDoList = document.getElementsByClassName('to-do-list')[0]
let newTaskName = document.getElementById('task-name')
let filteredList
let filterField = document.getElementById('drop-down-box')
filterField.addEventListener('change', function() {
    activeTask = allTask.filter(item => item.owner == JSON.parse(localStorage.currentUser))
    switch (filterField.selectedIndex) {
        case 0:
            filteredList = activeTask
            render(filteredList)
            break
        case 1:
            filteredList = activeTask.filter(item => item.isDone == true)
            render(filteredList)
            break
        case 2:
            filteredList = activeTask.filter(item => item.isDone == false)
            render(filteredList)
            break
    }
})

function render(taskList) {
    console.log(taskList)
    toDoList.innerHTML = ''
    for (let i = 0; i < taskList.length; i++) {
        let div = document.createElement('div')
        div.id = taskList[i].id
        let checkBox = document.createElement('input')
        checkBox.type = 'checkbox'
        checkBox.checked = taskList[i].isDone
        if (taskList[i].isDone)
            div.className = 'completed-tasks'
        else
            div.className = ""
        checkBox.addEventListener('click', () => {
            taskList[i].isDone = checkBox.checked
            for (let j = 0; j < allTask.length; j++) {
                if (allTask[j].id == taskList[i].id) {
                    allTask[j].isDone = checkBox.checked
                }
            }
            if (taskList[i].isDone)
                div.className = 'completed-tasks'
            else
                div.className = ""
            localStorage.setItem('task', JSON.stringify(allTask))
                //filterField.dispatchEvent(new Event("change"))
        })
        let task = document.createElement('span')
        let editButton = document.createElement('input')
        editButton.type = 'button'
        editButton.value = 'Edit'
        editButton.addEventListener('click', () => {
            chosenTask = taskList[i]
            newTaskName.value = taskList[i].taskName
            let updateButton = document.getElementById('add-update-button')
            updateButton.value = 'Update'
        })

        let deleteButton = document.createElement('input')
        deleteButton.type = 'button'
        deleteButton.value = 'Delete'
        deleteButton.addEventListener('click', function() {
            let confirmString = prompt('Do you REALLY want to delete? Process cannot be reversed\nType \'yes\' to confirm, any other string to discard')
            confirmString = confirmString.split('').map(item => item.toLowerCase()).join('')
            if (confirmString === 'yes') {
                parent = deleteButton.parentNode.id
                console.log('parent la', parent)
                const data = {
                    currentTask: parent,
                    currentUser: JSON.parse(localStorage.currentUser)
                }
                let xhttp = new XMLHttpRequest();
                xhttp.open("POST", "http://localhost:8080/delete-task", true)
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        getAllTask()
                    }
                };
                xhttp.send(JSON.stringify(data));
                // let b = allTask.filter(item => item.id != parent.id)
                // localStorage.setItem('task', JSON.stringify(b))
                // allTask = JSON.parse(localStorage.task)
                // filterField.dispatchEvent(new Event("change"))
                // cancelButton.dispatchEvent(new Event("click"))
            }
        })
        task.innerHTML = taskList[i].taskName
        div.appendChild(checkBox)
        div.appendChild(task)
        div.appendChild(editButton)
        div.appendChild(deleteButton)
        toDoList.appendChild(div)
    }
}
//render(activeTask)
let cancelButton = document.getElementById('cancel-button')
cancelButton.addEventListener('click', function() {
    addButton.value = 'Add'
    newTaskName.value = ''
})

let addButton = document.getElementById('add-update-button')
addButton.addEventListener('click', handleAddTask)

function handleAddTask() {
    console.log('go here 1', newTaskName)
    if (addButton.value == 'Update') {
        console.log('go here')
        let newTaskName = document.getElementById('task-name')
        if (newTaskName.value.length > 0) {
            chosenTask.taskName = newTaskName.value
            console.log('chosen id la', chosenTask.id)
            const data = {
                taskName: newTaskName.value,
                id: chosenTask.id
            }
            let xhttp = new XMLHttpRequest();
            xhttp.open("POST", "http://localhost:8080/edit-task", true)
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    getAllTask()
                }
            };
            xhttp.send(JSON.stringify(data));
            // for (let j = 0; j < allTask.length; j++) {
            //     if (allTask[j].id == chosenTask.id) {
            //         allTask[j].taskName = chosenTask.taskName
            //     }
            // }
            // localStorage.setItem('task', JSON.stringify(allTask))
            // activeTask = allTask.filter(item => item.owner == JSON.parse(localStorage.currentUser))
            // filterField.dispatchEvent(new Event("change"))
            // cancelButton.dispatchEvent(new Event("click"))
        }
    } else if (addButton.value == 'Add') {
        let newTaskName = document.getElementById('task-name')
        if (newTaskName.value.length > 0) {
            const data = {
                taskName: newTaskName.value,
                isDone: false,
                owner: JSON.parse(localStorage.currentUser)
            }
            let xhttp = new XMLHttpRequest();
            xhttp.open("POST", "http://localhost:8080/add-task", true)
            xhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                    getAllTask()
                }
            };
            xhttp.send(JSON.stringify(data));
        }
        newTaskName.value = ''
        addButton.value = 'Add'
    }
    newTaskName.value = ''
    addButton.value = 'Add'
}

var signOutButton = document.getElementById('sign-out-button')
signOutButton.addEventListener('click', signOut)

function signOut() {
    localStorage.removeItem('currentUser')
    location.href = "./login.html"
}

function getAllTask() {
    let xhttp = new XMLHttpRequest();
    const data = {
        currentUser: JSON.parse(localStorage.currentUser)
    }
    xhttp.open("POST", "http://localhost:8080/get-task", true)
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let activeTask = JSON.parse(this.responseText)
                //let activeTask = allTask.filter(item => item.owner == JSON.parse(localStorage.currentUser))
            render(activeTask)
        }
    };
    xhttp.send(JSON.stringify(data));

}
getAllTask()
    // let newTaskName = document.getElementById('task-name')
    // console.log(newTaskName.value)
    // if (newTaskName.value.length > 0) {
    //     let currentId = allTask[allTask.length - 1].id
    //     currentId = (parseInt(currentId) + 1).toString()
    //     allTask.push({ id: currentId, taskName: newTaskName.value, isDone: false, owner: JSON.parse(localStorage.currentUser) })
    //     localStorage.setItem('task', JSON.stringify(allTask))
    //     allTask = JSON.parse(localStorage.task)
    //     activeTask = allTask.filter(item => item.owner == JSON.parse(localStorage.currentUser))
    //     render(activeTask)
    ///////render
    // let div = document.createElement('div')
    // div.id = allTask.length
    // let checkbox = document.createElement('input')
    // checkbox.type = 'checkbox'
    // let task = document.createElement('span')
    // let editButton = document.createElement('button')
    // editButton.innerHTML = 'Edit'
    // let deleteButton = document.createElement('button')
    // deleteButton.innerHTML = 'Delete'

// task.innerHTML = newTaskName.value
// div.appendChild(checkbox)
// div.appendChild(task)
// div.appendChild(editButton)
// div.appendChild(deleteButton)
// toDoList.appendChild(div)
//}