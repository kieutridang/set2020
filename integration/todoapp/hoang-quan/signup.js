var users = [{
    id: '0',
    username: 'admin',
    password: '123'
}, {
    id: '1',
    username: 'quan',
    password: '123'
}, {
    id: '2',
    username: 'huy',
    password: '123',
}, {
    id: '3',
    username: 'hoang',
    password: '123'
}]
if (localStorage.getItem('currentUser') != null)
    location.href = "./main.html"
if (localStorage.getItem('users') == null)
    localStorage.setItem('users', JSON.stringify(users))

var tasks = [{
    id: '0',
    taskName: 'study',
    isDone: false,
    owner: '1'
}, {
    id: '1',
    taskName: 'sleep',
    isDone: true,
    owner: '0'
}]
if (localStorage.getItem('task') == null)
    localStorage.setItem('task', JSON.stringify(tasks))
var allUsers = JSON.parse(localStorage.users)

const body = document.getElementsByTagName('body')[0]
const error = document.getElementById('footer')
body.appendChild(error)
error.innerHTML = ''
error.style.color = 'red'
var back = document.getElementById('form__back-button')
var usernameNode = document.getElementById('form__username')
var passwordNode = document.getElementById('form__password')
var confirmPassword = document.getElementById('form__confirm-password')
back.addEventListener('click', function() {
    location.href = ("./login.html")
})
var submitSignUp = document.getElementById('form__sign-up-button')
submitSignUp.addEventListener('click', handleSignUp)

function validateUsername(usernameNode) {
    let flag = true
    let input = usernameNode.value
    if (input.length < 2) {
        error.innerHTML = error.innerHTML + 'username phai co it nhat 8 ki tu <br/>'
        flag = false
    }
    // check ki tu dac biet
    let check = null
    check = input.match(/[\W]/g)
    if (check != null) {
        error.innerHTML = error.innerHTML + 'username khong duoc co ki tu dac biet hay khoang trang <br/>'
        flag = false
    }
    if (!flag) {
        usernameNode.style.borderColor = 'red'
    }
    return flag
}

function validatePassword(passwordNode) {
    let flag = true
    let input = passwordNode.value
    let check
    if (input.length < 2) {
        console.log('herer')
        error.innerHTML = error.innerHTML + 'password phai co it nhat 8 ki tu <br/>'
        flag = false
    }
    // check ki tu dac biet
    check = null
    check = input.match(/[\W]/g)
    if (check != null) {
        error.innerHTML = error.innerHTML + 'pass khong duoc co ki tu dac biet <br/>'
        flag = false
    }
    if (!flag) {
        passwordNode.style.borderColor = 'red'
        return flag
    }
    if (input != confirmPassword.value) {
        error.innerHTML = error.innerHTML + 'password va confirm password phai giong nhau <br/>'
        confirmPassword.style.borderColor = 'red'
        flag = false
    }
    return flag
}

function handleSignUp() {
    error.innerHTML = ''
    let isUsernameValid = validateUsername(usernameNode)
    let isPasswordValid = validatePassword(passwordNode)
    if (isUsernameValid === true && isPasswordValid === true) {

        // let currentId = allUsers.length
        // currentId = currentId.toString()
        // allUsers.push({ id: currentId, username: usernameNode.value, password: passwordNode.value })
        // localStorage.setItem('users', JSON.stringify(allUsers))
        // localStorage.setItem('currentUser', JSON.stringify(currentId))
        let xhttp = new XMLHttpRequest();
        const data = {
            username: usernameNode.value,
            password: passwordNode.value
        }
        console.log(data)
        xhttp.open("POST", "http://localhost:8080/sign-up", true)
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log('backend return ', JSON.parse(this.responseText))
                if (JSON.parse(this.responseText) == 'deny') {
                    console.log('false')
                    error.innerHTML = error.innerHTML + 'username bi trung <br/>'
                    usernameNode.style.borderColor = 'red'
                } else {
                    console.log('true')
                    localStorage.setItem('currentUser', JSON.stringify(JSON.parse(this.responseText)))
                    location.href = "./main.html"
                }
            }
        };
        xhttp.send(JSON.stringify(data));
    }
}