const fs = require('fs').promises
const crypto = require('crypto')
const http = require('http'); // buoc 1
const server = http.createServer((request, response) => {
    if (request.method == "GET") {

    } else {
        const chunks = []
        request.on('data', (chunk) => { //event data  - buoc 2
            chunks.push(chunk)
        }).on('end', () => {
            let bodyData = Buffer.concat(chunks).toString()
            bodyData = JSON.parse(bodyData)
            if (request.url == '/edit-task' && request.method == 'POST') {
                console.log('go edit')
                getTask()
                    .then(data => {
                        allTask = data
                        for (let i = 0; i < allTask.length; i++) {
                            if (allTask[i].id == bodyData.id) {
                                allTask[i].taskName = bodyData.taskName
                            }
                        }
                        fs.writeFile('./tasks.txt', JSON.stringify(allTask)).catch(err => console.error(err))
                        response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                        response.end()

                    })
                    .catch(err => console.error(err))
            }
            if (request.url == '/delete-task' && request.method == 'POST') {
                getTask()
                    .then(data => {
                        allTask = data
                        let newAllTask = allTask.filter(item => item.id != bodyData.currentTask)
                        fs.writeFile('./tasks.txt', JSON.stringify(newAllTask)).catch(err => console.error(err))
                        response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                        response.end()
                    })
                    .catch(err => console.error(err))
            }
            if (request.url == '/get-task' && request.method == 'POST') {
                getTask()
                    .then(data => {
                        let allTask = data
                        console.log(bodyData)
                        console.log('alltask la', allTask)
                        let activeTask = allTask.filter(item => item.owner == bodyData.currentUser)
                        response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                        response.write(JSON.stringify(activeTask))
                        response.end()
                    })
                    .catch(err => console.error(err))
            }
            getUser()
                .then(data => {
                    allUsers = data
                    if (request.url == '/sign-in' && request.method == 'POST') {
                        console.log('go sign in')
                        console.log('data is', bodyData)
                        let flag = false
                        for (let i = 0; i < allUsers.length; i++) {
                            if (allUsers[i].username == bodyData.username && allUsers[i].password == bodyData.password) {
                                flag = true
                                console.log('user sign in id - ', allUsers[i].id)
                                response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                                response.write(JSON.stringify(allUsers[i].id));
                                response.end()
                            }
                        }
                        if (!flag) {
                            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                            response.write(JSON.stringify('deny'));
                            response.end()
                        }
                    } else if (request.url == '/sign-up' && request.method == 'POST') {
                        console.log('go sign up')
                        let flag = true
                        for (let i = 0; i < allUsers.length; i++) {
                            if (allUsers[i].username == bodyData.username) {
                                flag = false
                                break;
                            }
                        }
                        if (flag) {
                            let newUser = {
                                id: allUsers.length.toString(),
                                username: bodyData.username,
                                password: bodyData.password
                            }
                            allUsers.push(newUser)
                            console.log('user after add', allUsers)
                            fs.writeFile('./users.txt', JSON.stringify(allUsers)).catch(err => console.error(err))
                            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                            response.write(JSON.stringify(allUsers.length - 1));
                            response.end()
                        } else {
                            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                            response.write(JSON.stringify('deny'));
                            response.end()
                        }
                    } else if (request.url == '/add-task' && request.method == 'POST') {
                        getTask()
                            .then(data => {
                                allTask = data
                                tempId = Number(allTask[allTask.length - 1].id) + 1
                                const newTask = {
                                    id: tempId.toString(),
                                    taskName: bodyData.taskName,
                                    isDone: bodyData.isDone,
                                    owner: bodyData.owner
                                }
                                allTask.push(newTask)
                                console.log('task after add', allTask)
                                response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
                                fs.writeFile('./tasks.txt', JSON.stringify(allTask)).catch(err => console.error(err))
                                    //response.write(JSON.stringify(allUsers.length - 1));
                                response.end()
                            })
                            .catch(err => console.error(err))
                    }
                })
                .catch(err => console.error(err))
        })
    }
}).listen(8080, () => {
    console.log('server start')
});

function getUser() {
    return fs.readFile('./users.txt')
        .then(data => JSON.parse(data))
}

function getTask() {
    return fs.readFile('./tasks.txt')
        .then(data => JSON.parse(data))
}
// var tasks = [{
//     id: '0',
//     taskName: 'hoc bai',
//     isDone: false,
//     owner: '0'
// }, {
//     id: '1',
//     taskName: 'di hoc',
//     isDone: true,
//     owner: '1'
// }]
// fs.writeFile('./tasks.txt', JSON.stringify(tasks)).catch(err => console.error(err))