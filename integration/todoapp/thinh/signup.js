let users
fetch('http://localhost:8080/sign-up', {
  method: 'GET'
})
  .then(response => response.json())
  .then(data => {
    if (data.status === 'success') {
      localStorage.setItem('users', JSON.stringify(data.users))
    } else {
      localStorage.setItem('users', '')
    }
    users = JSON.parse(localStorage.getItem('users'))
    console.log('load success')
  })
  .catch(err => console.log(err))

let form = document.getElementsByClassName('form-container')[0]
form.addEventListener('submit', handleSubmitSignUp)

let backButton = document.getElementsByClassName('__back-button')[0]
backButton.addEventListener('click', redirectToLoginPage)

let errors = {
  isUnique: false,
  hasEnoughCharacters: false,
  hasSpecialCharacter: false,
  hasWhiteSpace: false
}

function handleSubmitSignUp(e) {
  e.preventDefault()
  console.log('say something')
  let errorMessages = document.getElementsByClassName('__error-message')[0]
  errorMessages.innerHTML = ''
  let usernameInput = document.getElementsByClassName('__username-input')[0].value
  let usernameError = validateInput(usernameInput, users)
  if (usernameError.isUnique === false) {
    errorMessages.innerHTML += 'Username already existed <br/>'
  }
  if (usernameError.hasEnoughCharacters === false) {
    errorMessages.innerHTML += 'Username must have more than 8 characters <br/>'
  }
  if (usernameError.hasSpecialCharacter === true) {
    errorMessages.innerHTML += 'Username must not contain special character <br/>'
  }

  let passwordInput = document.getElementsByClassName('__password-input')[0].value
  let passwordError = validateInput(passwordInput, users)  
  if (passwordError.hasEnoughCharacters === false) {
    errorMessages.innerHTML += 'Password must have more than 8 characters <br/>'
  }
  if (passwordError.hasSpecialCharacter === true) {
    errorMessages.innerHTML += 'Password must not contain special character <br/>'
  } 
  if (passwordError.hasWhiteSpace === true) {
    errorMessages.innerHTML += 'Password must not contain white space <br/>'
  }
  let confirmPassword = document.getElementsByClassName('__confirm-password-input')[0].value
  if (confirmPassword !== passwordInput) {
    errorMessages.innerHTML += 'Confirm password does not match <br/>'
  }

  if (errorMessages.innerHTML === '') {
    const bodyData = {
      username: usernameInput,
      password: passwordInput
    }
    fetch('http://localhost:8080/sign-up', {
      method: 'POST',
      body: JSON.stringify(bodyData)
    })
      .then(response => response.json())
      .then(data => {
        if (data.status === 'success') {
          localStorage.setItem('currentUser', JSON.stringify(data.user))
          window.location.href = './main.html'
        }
      })
      .catch(err => console.log(err))
  }
}

function validateInput(input, users) {
  let errors = {
    isUnique: false,
    hasEnoughCharacters: false,
    hasSpecialCharacter: true,
    hasWhiteSpace: false
  }
  if (users.find(item => item.username === input)) {
    errors.isUnique = false
  } else {
    errors.isUnique = true
  }
  if (input.match(/^.{8,}$/)) {
    errors.hasEnoughCharacters = true
  } else {
    errors.hasEnoughCharacters = false
  }
  if (input.match(/\W/)) {
    errors.hasSpecialCharacter = true
  } else {
    errors.hasSpecialCharacter = false
  }
  if (input.match(/\s/)) {
    errors.hasWhiteSpace = true
  } else {
    errors.hasWhiteSpace = false
  }
  return errors
}
function redirectToLoginPage() {
  location.href = './login.html'
}
function checkCurrentUser() {
  let currentUser = JSON.parse(localStorage.getItem("currentUser")) || {}
  console.log(currentUser)
  if (currentUser.id) {
    window.location.href = "./main.html"
  }
}
checkCurrentUser()