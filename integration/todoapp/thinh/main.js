fetch('http://localhost:8080/main', {
	method: 'POST',
	body: localStorage.getItem('currentUser'),
})
	.then((response) => response.json())
	.then((data) => {
		if (data.status === 'success') {
			localStorage.setItem('tasks', JSON.stringify(data.tasks))
		} else {
			localStorage.setItem('tasks', '')
		}
		console.log('load success')
	})
	.then(() => {
		return getCurrentUsersTasks()
	})
	.then((initialTaskList) => renderTaskList(initialTaskList))
	.catch((err) => console.log(err))
console.log(localStorage.getItem('tasks'))

//get current user from localStorage
let currentUser = localStorage.getItem('currentUser')
currentUser = JSON.parse(currentUser)

//filter task that belong to current user and render
// let initialTaskList = getCurrentUsersTasks()
// let initialRender = renderTaskList(initialTaskList) //initial render

//get HTML elements
let taskNameInput = document.getElementsByClassName('task-name-input')[0] // HTML element input field
let addUpdateButton = document.getElementsByClassName('add-update-button')[0] //HTML element Add/Update button
let cancelButton = document.getElementsByClassName('cancel-button')[0]
let filterField = document.getElementsByClassName('drop-down-box')[0]

let chosenTask
let filterType = 0
//function: filter
let filteredList
//add event listener
//function: filter
filterField.addEventListener('change', function () {
	let currentTasks = getCurrentUsersTasks()
	filterType = filterField.selectedIndex
	let filteredTaskList = getTasksBasedOnFilterType(currentTasks, filterType)
	renderTaskList(filteredTaskList)
})
//function: Cancel Input
cancelButton.addEventListener('click', resetInputAndAddButton)
function resetInputAndAddButton() {
	addUpdateButton.value = 'Add'
	taskNameInput.value = ''
	taskNameInput.placeHolder = 'Task name...'
}
//function: edit
addUpdateButton.addEventListener('click', handleAddUpdate)
function handleAddUpdate() {
	if (addUpdateButton.value === 'Add') {
    fetch('http://localhost:8080/main-add', {
      method: 'POST',
      body: JSON.stringify({
        id: '',
        taskName: taskNameInput.value,
        isDone: false,
        owner: currentUser.id
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.status === 'success') {
          console.log(data)
          console.log('before', localStorage.getItem('tasks'))
          localStorage.setItem('tasks', JSON.stringify(data.tasks))
          console.log('after', localStorage.getItem('tasks'))

        }
      })
      .then(() => console.log('hix'))
      .then(() => onChangeRender())
      .catch(err => console.log(err))
		// let addedTask = {
		// 	id: '',
		// 	taskName: taskNameInput.value,
		// 	isDone: false,
		// 	owner: currentUser.id,
		// }
		// addToLocalStorage('tasks', addedTask)

		//get current task to render
	} else if (addUpdateButton.value === 'Update') {
		chosenTask.taskName = taskNameInput.value
		let updatedList = JSON.parse(localStorage.getItem('tasks'))
		updatedList = updatedList.map((item) =>
			item.id === chosenTask.id ? chosenTask : item
		)
		updateLocalStorage('tasks', updatedList)
		onChangeRender()
	}
}
function onChangeRender() {
	let taskList = getCurrentUsersTasks()
	taskList = getTasksBasedOnFilterType(taskList, filterType)
	renderTaskList(taskList)
	resetInputAndAddButton()
}
function getCurrentUsersTasks() {
	return JSON.parse(localStorage.getItem('tasks')).filter(
		(item) => item.owner == currentUser.id
	)
}
function getTasksBasedOnFilterType(tasks, type) {
	switch (type) {
		case 1:
			return tasks.filter((item) => item.isDone === true)
		case 2:
			return tasks.filter((item) => item.isDone === false)
		default:
			return tasks
	}
}
//add 1 value to localStorage
function addToLocalStorage(key, newValue) {
	let value = JSON.parse(localStorage.getItem(key))
	if (newValue.id == '') {
		newValue.id = value[value.length - 1].id + 1
	}
	value.push(newValue)
	localStorage.setItem(key, JSON.stringify(value))
}
//replace a whole field by another field in localStorage
function updateLocalStorage(key, newValue) {
	localStorage.setItem(key, JSON.stringify(newValue))
}
function renderTaskList(taskList) {
	//get HTML element to append child
	let taskListContainer = document.getElementsByClassName(
		'__task-list-container'
	)[0]
	taskListContainer.innerHTML = ''
	//for each task in current list, create an HTML element and append to taskListContainer
	return taskList.map((item, index) => {
		let container = document.createElement('div')
		container.className = 'task-detail-container'
		container.id = item.id

		let checkBox = document.createElement('input')
		checkBox.className = '__check-box'
		checkBox.type = 'checkbox'
		checkBox.checked = item.isDone
		//event click to change status of a task (toggle between done and undone)
		checkBox.addEventListener('click', () => {
			chosenTask = item
			chosenTask.isDone = checkBox.checked
			let updatedList = JSON.parse(localStorage.getItem('tasks'))
			updatedList = updatedList.map((item1) =>
				item1.id === chosenTask.id ? chosenTask : item1
			)
			updateLocalStorage('tasks', updatedList)
			let taskList = getCurrentUsersTasks()
			taskList = getTasksBasedOnFilterType(taskList, filterType)
			renderTaskList(taskList)
		})
		container.appendChild(checkBox)

		let taskName = document.createElement('label')
		taskName.className = '__task-name'
		taskName.innerHTML = item.taskName
		container.appendChild(taskName)

		let editButton = document.createElement('input')
		editButton.className = '__edit-button'
		editButton.type = 'button'
		editButton.value = 'Edit'
		//event handler for click edit button
		editButton.addEventListener('click', () => {
			chosenTask = item
			taskNameInput.value = item.taskName
			console.log(item.taskName)
			let updateButton = document.getElementsByClassName('add-update-button')[0]
			updateButton.value = 'Update'
		})
		container.appendChild(editButton)

		let deleteButton = document.createElement('input')
		deleteButton.className = '__delete-button'
		deleteButton.type = 'button'
		deleteButton.value = 'Delete'
		//event handler for click delete button
		deleteButton.addEventListener('click', () => {
			let confirmString = prompt(
				"Do you REALLY want to delete? Process cannot be reversed\nType 'yes' to confirm, any other string to discard"
			)
			confirmString = confirmString
				.split('')
				.map((item) => item.toLowerCase())
				.join('')
			if (confirmString === 'yes') {
				taskList = JSON.parse(localStorage.getItem('tasks'))
				//get id of clicked task
				let i = item.id
				//get object with that id
				let taskToDelete = taskList.find((item1) => item1.id == i)
				//filter current task list to remove the deleted object
				let updatedList = taskList.filter((item) => item != taskToDelete)
				//reRender the whole task list
				updateLocalStorage('tasks', updatedList)
				updatedTasksList = localStorage.getItem('tasks')
				updatedTasksList = JSON.parse(updatedTasksList).filter((item) =>
					filterType == '0'
						? item.owner == currentUser.id
						: filterType == '1'
						? item.owner == currentUser.id && item.isDone == true
						: item.owner == currentUser.id && item.isDone == false
				)
				renderTaskList(updatedTasksList)
			} else {
				console.log('no')
			}
		})
		container.appendChild(deleteButton)
		taskListContainer.appendChild(container)
	})
}
