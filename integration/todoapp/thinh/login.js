let usernameElement = document.getElementsByClassName('__username-input')[0]
let passwordElement = document.getElementsByClassName('__password-input')[0]
let form = document.getElementsByClassName('form-container')[0]
form.addEventListener('submit', handleSubmitLogin)
function handleSubmitLogin(e) {
  console.log('clicked')
  e.preventDefault()
  const bodyData = {
    username: usernameElement.value,
    password: passwordElement.value
  }
  fetch('http://localhost:8080/sign-in', {
    method: 'POST',
    body: JSON.stringify(bodyData)
  })
    .then(response => response.json())
    .then(data => {
      if (data.status == 'success') {
        console.log(data)
        console.log('status: success')
        localStorage.setItem('currentUser', JSON.stringify(data.user))
        window.location.href = './main.html'
      } else {
        alert('invalid username or password')
      }
    })
    .catch(err => {
      console.log(err)
    })
}
let signupButton = document.getElementsByClassName('__sign-up-button')[0]
signupButton.addEventListener('click', redirectToSignupPage)
function redirectToSignupPage() {
  location.href = './signup.html'
}
function checkCurrentUser() {
  let currentUser = JSON.parse(localStorage.getItem("currentUser")) || {}
  console.log(currentUser)
  if (currentUser.id) {
    window.location.href = "./main.html"
  }
}
checkCurrentUser()