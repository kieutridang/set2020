const fs = require('fs').promises
const crypto = require('crypto')
const http = require('http')

const server = http.createServer((request, response) => {
  const chunks = []
  request
    .on('data', chunk => {
    chunks.push(chunk)
  })
    .on('end', () => {
      if (request.url === '/sign-in' && request.method === 'POST') {
        let bodyData = Buffer.concat(chunks).toString()
        if (bodyData) {
          bodyData = JSON.parse(bodyData)
          bodyData.password = crypto.createHash('sha256').update(bodyData.password).digest('hex')
        }
        let responseData = {}
        fs.readFile('./users.txt', 'utf8')  
          .then(data => {
            data = JSON.parse(data)
            let matchAccount = data.find(item => item.username == bodyData.username && item.password == bodyData.password)
            if (matchAccount) {
              responseData.status = 'success'
              responseData.user = {
                id: matchAccount.id,
                username: matchAccount.username
              }
            } else {
              responseData.status = 'fail'
            }
            response.writeHead(200, { "Access-Control-Allow-Origin": "*" });
            response.write(JSON.stringify(responseData));
            response.end();
          })
          .catch(err => console.log(err))
      }
      else if (request.url === '/sign-up' && request.method === 'GET') {
        let responseData = {}
        fs.readFile('./users.txt', 'utf8')
          .then(data => {
            data = JSON.parse(data)
            responseData.status = 'success'
            responseData.users = data.reduce((total, current) => {
              let userData = {
                id: current.id,
                username: current.username
              }
              total.push(userData)
              return total
            }, [])
            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
            response.write(JSON.stringify(responseData))
            response.end();
          })
          .catch(err => console.log(err))
      }
      else if (request.url === '/sign-up' && request.method === 'POST') {
        let bodyData = Buffer.concat(chunks).toString()
        if (bodyData) {
          bodyData = JSON.parse(bodyData)
          bodyData.password = crypto.createHash('sha256').update(bodyData.password).digest('hex')
        }
        let responseData = {}
        fs.readFile('./users.txt', 'utf8')
          .then(data => {
            data = JSON.parse(data)
            let newUser = {
              id: data.length + 1,
              username: bodyData.username,
              password: bodyData.password
            }
            responseData.status = 'success'
            responseData.user = {
              id: newUser.id,
              username: newUser.username
            }
            data.push(newUser)
            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
            response.write(JSON.stringify(responseData))
            return data
          })
          .then(data => {
            fs.writeFile('./users.txt', JSON.stringify(data))
              .catch(err => console.log(err))
          })
          .then(() => {
            response.end()
          })
          .catch()
      }
      else if (request.url === '/main' && request.method === 'POST') {
        let bodyData = Buffer.concat(chunks).toString()
        if (bodyData) {
          bodyData = JSON.parse(bodyData)
        }
        let responseData = {}
        fs.readFile('./tasks.txt', 'utf8')
          .then(data => {
            data = JSON.parse(data)
            responseData.status = 'success'
            responseData.tasks = data.filter(item => item.owner == bodyData.id)
            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
            response.write(JSON.stringify(responseData))
            response.end();
          })
          .catch(err => console.log(err))
      }
      else if (request.url === '/main-add' && request.method === 'POST') {
        let bodyData = Buffer.concat(chunks).toString()
        if (bodyData) {
          bodyData = JSON.parse(bodyData)
        }
        let responseData = {}
        fs.readFile('./tasks.txt', 'utf8')
          .then(data => {
            data = JSON.parse(data)
            let newTask = {
              id: data.length + 1,
              taskName: bodyData.taskName,
              isDone: bodyData.isDone,
              owner: bodyData.owner
            }
            data.push(newTask)
            responseData.status = 'success'
            responseData.tasks = data.filter(item => item.owner == bodyData.owner)
            response.writeHead(200, { "Access-Control-Allow-Origin": "*" })
            response.write(JSON.stringify(responseData))
            return data
          })
          .then(data => {
            fs.writeFile('./tasks.txt', JSON.stringify(data))
              .catch(err => console.log(err))
          })
          .then(() => {
            response.end()
          })
          .catch(err => console.log(err))
      }
       
  })
}).listen(8080, () => {
  console.log('server started')
})
