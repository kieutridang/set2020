const userId = window.location.search.split("=")[1];
const taskField = document.getElementById("tasks");
const inputTaskField = document.getElementById("todo-input");
let allTasks = []
// JSON.parse(localStorage.getItem("tasks")) || [];
let userTasks = allTasks.filter((task) => task.userId === userId);
fetch('http://localhost:8080/main', {
    method: 'POST'
}).then(response => {
    return response.json()
}).then(data => {
    console.log(data)
    allTasks = data || []
    return allTasks
}).then(allTasks => {
    render(allTasks)
}).catch(err => console.log(err))
function render(allTasks) {
    console.log(allTasks)
    let userTasks = allTasks.filter((task) => task.userId === userId);
    console.log(userTasks)
    const statusFilter = document.getElementById("status-filter").value;
    if (statusFilter === "done") {
        userTasks = allTasks.filter(
            (task) => task.userId === userId && task.isDone === true
        );
    } else if (statusFilter === "undone") {
        userTasks = allTasks.filter(
            (task) => task.userId === userId && task.isDone === false
        );
    } else {
        userTasks = allTasks.filter((task) => task.userId === userId);
    }
    taskField.innerHTML = userTasks
        .map((task) => {
            return `<li key=${task.id} isEditing= "false">
                    <input type="checkbox" ${
                task.isDone ? "checked" : ""
                } onchange="toggleStatus(${task.id})"/>
                    <input type="text" value=${
                task.taskName
                } disabled id="input-${task.id}"/>
                    <button onclick="editTask(${task.id})">edit</button>
                    <button onclick="deleteTask(${task.id})">delete</button>
                </li>`;
        })
        .join("");
}
function addTask() {
    let newTask = {
        id: allTasks.length,
        taskName: inputTaskField.value,
        isDone: false,
        userId: userId,
    };
    allTasks.push(newTask);
    // userTasks.push(newTask);
    console.log(newTask)
    // console.log(allTasks)
    // localStorage.setItem("tasks", JSON.stringify(allTasks));
    fetch('http://localhost:8080/addtask', {
        method: 'POST',
        body: JSON.stringify(allTasks)
    }).catch(err => console.log(err))
    render(allTasks)
    inputTaskField.value = "";
}

function toggleStatus(taskId) {
    allTasks = allTasks.map((task) => {
        if (task.id === taskId) {
            task.isDone = !task.isDone;
        }
        return task;
    });
    localStorage.setItem("tasks", JSON.stringify(allTasks));
    render();
}
function editTask(taskId) {
    input = document.getElementById(`input-${taskId}`);
    inputTaskField.value = input.value
    console.log(input)
    console.log(inputTaskField)
    inputTaskField.focus()
    let addButton = document.getElementById("container-buttonAdd")
    let updateButton = document.getElementById("update-button")
    let cancelButton = document.getElementById("cancel-button")
    updateButton.style.display = "inline-block"
    cancelButton.style.display = "inline-block"
    addButton.style.display = "none"
    updateButton.addEventListener("click", function () {
        input.value = inputTaskField.value

        allTasks = allTasks.map((task) => {
            if (task.id === taskId) {
                task.taskName = input.value;
            }
            return task
        });
        localStorage.setItem("tasks", JSON.stringify(allTasks));
        render();
        updateButton.style.display = "none"
        cancelButton.style.display = "none"
        addButton.style.display = "inline-block"
        location.reload()
    })
    cancelButton.addEventListener("click", function () {
        inputTaskField.value = ""
        updateButton.style.display = "none"
        cancelButton.style.display = "none"
        addButton.style.display = "inline-block"
        location.reload()
    })
}
function deleteTask(taskId) {
    let confirm = prompt("do you really want to delete?, input 'yes' to delete.", "")
    if (confirm === "yes") {
        allTasks = allTasks.filter((task) => task.id !== taskId);
        for (let index = 0; index < allTasks.length; index++) {
            allTasks[index].id = index
        }
        console.log(allTasks)
        localStorage.setItem("tasks", JSON.stringify(allTasks));
    }
    render();
}
function toggleFilter() {
    render();
}