const form = document.getElementById("form")
form.addEventListener("submit", submitForm);

function submitForm(e) {
    e.preventDefault()
    const username = document.getElementById("username-field")
    const password = document.getElementById("password-field")
    let usernameErrorField = document.getElementById("username-errors")
    let passwordErrorField = document.getElementById("password-errors")
    const data = {
        username: username.value,
        password: password.value
    }
    // let users = JSON.parse(localStorage.getItem("users")) || []
    // const user = users.filter(user => user.username === username.value && user.password === password.value)
    // if (user.length) {
    //     window.location = `./main.html?id=${user[0].id}`
    // } else {
    //     usernameErrorField.innerHTML = `<div>Your username or password is incorrect</div>`
    // }
    fetch('http://localhost:8080/sign-in', {
        method: 'POST',
        body: JSON.stringify(data)
    })
        .then((response) => {
            return response.json()
        })
        .then(data => {
            console.log(data)
            if (data.status === 'success') {
                window.alert('sign in successful')
                window.location = `./main.html?id=${data.id}`
            }
            else {
                usernameErrorField.innerHTML = `<div>Your username or password is incorrect</div>`
                username.style.borderColor = 'red'
            }
        }).catch(err => console.log(err))
}