const http = require('http');
const fs = require('fs').promises;
const crypto = require('crypto')
let allUsers = []
const server = http.createServer((request, response) => {
    const chunks = []
    if (request.url === '/sign-up' && request.method === 'POST') {
        request.on('data', (chunk) => {
            chunks.push(chunk)
        }).on('end', () => {
            let user = Buffer.concat(chunks).toString()
            user = JSON.parse(user)
            fs.readFile('../../database/users.JSON')
                .then(data => {
                    if (data == "") {
                        allUsers = []
                    }
                    else {
                        allUsers = JSON.parse(data)
                    }
                    return allUsers
                })
                .then(allUsers => {
                    if (allUsers.every(item => item.username !== user.username)) {
                        user.password = crypto.createHash('sha256').update(user.password).digest('hex')
                        allUsers.push({ id: allUsers.length, username: user.username, password: user.password })
                        response.write(JSON.stringify({ id: allUsers.length - 1, status: 'success' }))
                    }
                    else {
                        response.write(JSON.stringify({ status: 'failed' }))
                    }
                    response.end()
                    return allUsers
                })
                .then(allUsers => {
                    fs.writeFile('../../database/users.JSON', JSON.stringify(allUsers))
                }).catch(err => console.error(err))
        })
    }
    else if (request.url === '/sign-in' && request.method === 'POST') {
        request.on('data', (chunk) => {
            chunks.push(chunk)
        }).on('end', () => {
            let user = Buffer.concat(chunks).toString()
            user = JSON.parse(user)
            user.password = crypto.createHash('sha256').update(user.password).digest('hex')
            fs.readFile('../../database/users.JSON')
                .then(data => {
                    if (data == "") {
                        allUsers = []
                    }
                    else {
                        allUsers = JSON.parse(data)
                    }
                    return allUsers
                })
                .then(allUsers => {
                    if (allUsers.some(item => item.username === user.username && item.password === user.password)) {
                        const currentUser = allUsers.find(item => item.username === user.username && item.password === user.password)
                        console.log(currentUser)
                        response.write(JSON.stringify({ id: currentUser.id, status: "success" }))
                    }
                    else {
                        response.write(JSON.stringify({ status: 'failed' }))
                    }
                    response.end()
                })
                .catch(err => console.error(err))
        })
    }
    else if (request.url === '/main' && request.method === 'POST') {
        fs.readFile('../../database/tasks.JSON')
            .then(data => {
                let allTasks = JSON.parse(data)
                response.write(JSON.stringify(allTasks))
                response.end()
            })
            .catch(err=> console.log(err))
    }
    else if (request.url === '/addtask' && request.method === 'POST') {
        request.on('data', (chunk) => {
            chunks.push(chunk)
            console.log(chunks)
        }).on('end', () => {
            let allTasks = Buffer.concat(chunks).toString()
            allTasks = JSON.parse(allTasks)
            console.log(allTasks)
            fs.writeFile('../../database/tasks.JSON', JSON.stringify(allTasks))
            .catch(err=> console.log(err))
        })
    }
    response.writeHead(200, { 'Access-Control-Allow-Origin': '*' })
}).listen(8080, () => {
    console.log('server start')
});
