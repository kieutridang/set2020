const form = document.getElementById("form")
form.addEventListener("submit", submitForm);

function submitForm(e) {
    e.preventDefault()
    const username = document.getElementById("username-field")
    const password = document.getElementById("password-field")
    const usernameValidation = validateUsername(username.value)
    const passwordValidation = validatePassword(password.value)
    let usernameErrorField = document.getElementById("username-errors")
    let passwordErrorField = document.getElementById("password-errors")
    let confirmPassword = document.getElementById("confirmPassword-field")
    let confirmPasswordErrorField = document.getElementById("confirmPassword-errors")
    confirmPasswordErrorField.innerHTML = null
    if (!usernameValidation.isValid) {
        usernameErrorField.innerHTML = usernameValidation.errors.map(error => {
            return `<div>${error}</div>`
        }).join("")
        username.className = 'error'
    } else {
        usernameErrorField.innerHTML = null
    }
    if (!passwordValidation.isValid) {
        passwordErrorField.innerHTML = passwordValidation.errors.map(error => {
            return `<div>${error}</div>`
        }).join("")
        password.className = 'error'
    } else {
        passwordErrorField.innerHTML = null
    }
    if (usernameValidation.isValid && passwordValidation.isValid && password.value === confirmPassword.value) {
        // let users = JSON.parse(localStorage.getItem("users")) || []
        // if (users.some(user => user.username === username.value)) {
        //     usernameErrorField.innerHTML = `<div>This username is already registered</div>`
        // } else {
        //     users.push({ id: users.length, username: username.value, password: password.value })
        //     localStorage.setItem("users", JSON.stringify(users))
        //     window.location = `./main.html?id=${users[users.length-1].id}`
        // }
        const userData = {
            username: username.value,
            password: password.value
        }
        fetch('http://localhost:8080/sign-up', {
            method: 'POST',
            body: JSON.stringify(userData)
        })
            .then( function(response){
                
                return response.json()
            }).then((data)=>{
                console.log(data)
                if(data.status === 'success'){
                    window.alert('sign up successful')
                    window.location = `./main.html?id=${data.id}`
                }
                else{
                    window.alert('this username is already registered')
                }
            })
            .catch(function (error) {
                console.log(error)
            })
    }
    else {
        confirmPasswordErrorField.innerHTML = `<div> confirm password is not correct</div>`
    }
}

function validateUsername(username) {
    let result = {
        isValid: true,
        errors: [],
    }
    if (!/[A-Za-z0-9\@\.]/.test(username)) {
        result.errors.push('username can not contain special characters')
    }
    if (/[\s]/.test(username)) {
        result.errors.push('username can not contain space character')
    }
    if (username.length < 8 || username.length > 24) {
        result.errors.push('username length must from 8-24 characters')
    }
    if (result.errors.length) {
        result.isValid = false
    }
    
    return result
}

function validatePassword(password) {
    let result = {
        isValid: true,
        errors: [],
    }
    // const lengthCheck = /^\w{8,24}$/
    // if (!lengthCheck.test(password)) {
    //     console.log(!lengthCheck.test(password))
    //     result.errors.push('Must from 8-24 characters')
    // }
    const specialCharacterCheck = /[A-Za-z0-9]/
    if (!specialCharacterCheck.test(password)) {
        result.errors.push('password can not have special characters')
    }
    if (password.length < 8 || password.length > 24) {
        result.errors.push('password length must from 8-24 characters')
    }
    if (result.errors.length) {
        result.isValid = false
    }
    return result
}