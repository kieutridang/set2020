if (localStorage.getItem('currentUser') != null)
    location.href = "./main.html"
const body = document.getElementsByTagName('body')[0]
const error = document.getElementById('footer')
body.appendChild(error)
error.innerHTML = ''
error.style.color = 'red'
var back = document.getElementById('form__back-button')
var usernameNode = document.getElementById('form__username')
var passwordNode = document.getElementById('form__password')
var confirmPassword = document.getElementById('form__confirm-password')
back.addEventListener('click', function() {
    location.href = ("./login.html")
})
var submitSignUp = document.getElementById('form__sign-up-button')
submitSignUp.addEventListener('click', handleSignUp)
window.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) submitSignUp.click();
})

function validateUsername(usernameNode) {
    let flag = true
    let input = usernameNode.value
        //check username length
    if (input.length < 8) {
        error.innerHTML = error.innerHTML + 'Username must contains more than 8 characters <br/>'
        flag = false
    }
    // check special character + space
    let check = null
    check = input.match(/[\W]/g)
    if (check != null) {
        error.innerHTML = error.innerHTML + 'Username cant contain special character or space <br/>'
        flag = false
    }
    if (!flag) {
        usernameNode.style.borderColor = 'red'
    }
    return flag
}

function validatePassword(passwordNode) {
    let flag = true
    let input = passwordNode.value
    let check
        //check password length
    if (input.length < 8) {
        error.innerHTML = error.innerHTML + 'Password must contains more than 8 characters <br/>'
        flag = false
    }
    // check special character + space
    check = null
    check = input.match(/[\W]/g)
    if (check != null) {
        error.innerHTML = error.innerHTML + 'Password cant contain special character or space  <br/>'
        flag = false
    }
    if (!flag) {
        passwordNode.style.borderColor = 'red'
        return flag
    }
    if (input != confirmPassword.value) {
        error.innerHTML = error.innerHTML + 'Wrong Confirm Password <br/>'
        confirmPassword.style.borderColor = 'red'
        flag = false
    }
    return flag
}

function handleSignUp() {
    error.innerHTML = ''
    let isUsernameValid = validateUsername(usernameNode)
    let isPasswordValid = validatePassword(passwordNode)
    if (isUsernameValid == true && isPasswordValid == true) {
        const userData = {
            username: usernameNode.value,
            password: passwordNode.value
        }
        fetch('http://localhost:8080/sign-up', {
                method: 'POST',
                body: JSON.stringify(userData)
            })
            .then((response) => response.json())
            .then((data) => {
                if (data.status == 'success') {
                    localStorage.setItem("currentUser", JSON.stringify(data.user))
                    window.alert('Sign Up Successful')
                    location.href = "./main.html"
                } else {
                    window.alert('This Username Is Already Registered')
                }
            })
            .catch((error) => { console.error("Sign Up Failed", error); })
    }
}