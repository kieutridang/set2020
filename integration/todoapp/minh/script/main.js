let chosenTask
if (localStorage.getItem('currentUser') == undefined)
    location.href = "./login.html"
var allTasks = []

function getTask() {
    fetch('http://localhost:8080/main', {
            method: 'POST'
        }).then((response) => response.json())
        .then(data => {
            localStorage.setItem('task', JSON.stringify(data))
            allTasks = JSON.parse(localStorage.task)
            render(allTasks.filter(item => item.owner == JSON.parse(localStorage.currentUser)))
        })
        .catch(err => console.log(err))
}
getTask()
    //Sign out
function signOut() {
    localStorage.removeItem('currentUser')
    location.href = "./login.html"
}
var activeTask = allTasks.filter(item => item.owner == JSON.parse(localStorage.currentUser))
var toDoList = document.getElementsByClassName('to-do-list')[0]
let newTaskName = document.getElementById('task-name')
let filteredList
let filterField = document.getElementById('drop-down-box')
filterField.addEventListener('change', function() {
    activeTask = allTasks.filter(item => item.owner == JSON.parse(localStorage.currentUser))
    switch (filterField.selectedIndex) {
        case 0:
            filteredList = activeTask
            render(filteredList)
            break
        case 1:
            filteredList = activeTask.filter(item => item.isDone == true)
            render(filteredList)
            break
        case 2:
            filteredList = activeTask.filter(item => item.isDone == false)
            render(filteredList)
            break
    }
})

function addTask() {
    let currentId = allTasks[allTasks.length - 1].id
    currentId = (parseInt(currentId) + 1).toString()
    let newTask = {
        id: currentId,
        taskName: newTaskName.value,
        isDone: false,
        owner: JSON.parse(localStorage.currentUser)
            // owner: localStorage.getItem('currentUser')
    };

    fetch("http://localhost:8080/create-task", {
            method: "POST",
            body: JSON.stringify(newTask),
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.status === "success") {
                console.log("Task created");
            }
        })
        .catch((error) => {
            console.error("Error create task", error);
        });
    getTask()
    allTasks = JSON.parse(localStorage.task)
    activeTask = allTasks.filter(item => item.owner == JSON.parse(localStorage.currentUser))
    render(activeTask)
    newTaskName.value = ''
}

function render(taskList) {
    toDoList.innerHTML = ''
    for (let i = 0; i < taskList.length; i++) {
        console.log(allTasks)
        let div = document.createElement('div')
        div.id = taskList[i].id
        let checkBox = document.createElement('input')
        checkBox.type = 'checkbox'
        checkBox.checked = taskList[i].isDone
        if (taskList[i].isDone)
            div.className = 'completed-tasks'
        else
            div.className = ""
        checkBox.addEventListener('click', () => {
            taskList[i].isDone = checkBox.checked
            for (let j = 0; j < allTasks.length; j++) {
                if (allTasks[j].id == taskList[i].id) {
                    allTasks[j].isDone = checkBox.checked
                }
            }
            if (taskList[i].isDone)
                div.className = 'completed-tasks'
            else
                div.className = ""
            localStorage.setItem('task', JSON.stringify(allTasks))
            filterField.dispatchEvent(new Event("change"))
        })
        let task = document.createElement('span')
        let editButton = document.createElement('input')
        editButton.type = 'button'
        editButton.value = 'Edit'
        editButton.addEventListener('click', () => {
            chosenTask = taskList[i]
            newTaskName.value = taskList[i].taskName
            let updateButton = document.getElementById('add-update-button')
            updateButton.value = 'Update'
        })

        let deleteButton = document.createElement('input')
        deleteButton.type = 'button'
        deleteButton.value = 'Delete'
        deleteButton.addEventListener('click', function() {
            let confirmString = prompt('Do you REALLY want to delete? Process cannot be reversed\nType \'yes\' to confirm, any other string to discard')
            confirmString = confirmString.split('').map(item => item.toLowerCase()).join('')
            if (confirmString === 'yes') {
                parent = deleteButton.parentNode
                let b = allTasks.filter(item => item.id != parent.id)
                localStorage.setItem('task', JSON.stringify(b))
                allTasks = JSON.parse(localStorage.task)
                filterField.dispatchEvent(new Event("change"))
                cancelButton.dispatchEvent(new Event("click"))
            }
        })
        task.innerHTML = taskList[i].taskName
        div.appendChild(checkBox)
        div.appendChild(task)
        div.appendChild(editButton)
        div.appendChild(deleteButton)
        toDoList.appendChild(div)
    }
}

let cancelButton = document.getElementById('cancel-button')
cancelButton.addEventListener('click', function() {
    addButton.value = 'Add'
    newTaskName.value = ''
})
let addButton = document.getElementById('add-update-button')
addButton.addEventListener('click', handleAddTask)

function handleAddTask() {
    if (addButton.value == 'Update') {
        let newTaskName = document.getElementById('task-name')
        if (newTaskName.value.length > 0) {
            chosenTask.taskName = newTaskName.value
            for (let j = 0; j < allTasks.length; j++) {
                if (allTasks[j].id == chosenTask.id) {
                    allTasks[j].taskName = chosenTask.taskName
                }
            }
            localStorage.setItem('task', JSON.stringify(allTasks))
            activeTask = allTasks.filter(item => item.owner == JSON.parse(localStorage.currentUser))
            filterField.dispatchEvent(new Event("change"))
            cancelButton.dispatchEvent(new Event("click"))
            updateDatabase()
        }
    } else if (addButton.value == 'Add') {
        let newTaskName = document.getElementById('task-name')
        if (newTaskName.value.length > 0) {
            addTask()
        }
    }
}
window.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) addButton.click();
})

function updateDatabase() {
    fetch('http://localhost:8080/change-task', {
        method: 'POST',
        body: JSON.stringify(allTasks)
    }).catch(err => console.log(err))
}