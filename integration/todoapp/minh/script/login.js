if (localStorage.getItem('currentUser') != null)
    location.href = "./main.html"


var signUp = document.getElementById('form__sign-up-button')
signUp.addEventListener('click', function() {
    location.href = "./signup.html"
})
var signIn = document.getElementById('form__log-in-button')
signIn.addEventListener('click', function() {
    let username = document.getElementById('form__username')
    let password = document.getElementById('form__password')
    const userData = {
        username: username.value,
        password: password.value
    }
    fetch("http://localhost:8080/sign-in", {
            method: "POST",
            body: JSON.stringify(userData),
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.status == "success") {
                localStorage.setItem("currentUser", JSON.stringify(data.user))
                location.href = "./main.html"
            } else {
                window.alert('Your Username or Password is incorrect')
            }
        })
        .catch((error) => { console.error("Sign Up Failed", error); })
})
window.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) signIn.click();
})