// var users = [{
//     id: '0',
//     username: 'admin',
//     password: '123'
// }, {
//     id: '1',
//     username: 'quan',
//     password: '123'
// }, {
//     id: '2',
//     username: 'huy',
//     password: '123',
// }, {
//     id: '3',
//     username: 'hoang',
//     password: '123'
// }]
if (localStorage.getItem('currentUser') != null)
    location.href = "./main.html"
    // if (localStorage.getItem('users') == null)
    //     localStorage.setItem('users', JSON.stringify(users))

// var tasks = [{
//     id: '0',
//     taskName: 'study',
//     isDone: false,
//     owner: '1'
// }, {
//     id: '1',
//     taskName: 'sleep',
//     isDone: true,
//     owner: '0'
// }]
// if (localStorage.getItem('task') == null)
//     localStorage.setItem('task', JSON.stringify(tasks))
// var allUsers = JSON.parse(localStorage.users)

var signUp = document.getElementById('form__sign-up-button')
signUp.addEventListener('click', function() {
    location.href = "./signup.html"
})
var signIn = document.getElementById('form__log-in-button')
signIn.addEventListener('click', function() {
    let username = document.getElementById('form__username')
    let password = document.getElementById('form__password')
    const userData = {
        username: username.value,
        password: password.value
    }
    fetch("http://localhost:8080/sign-in", {
            method: "POST",
            body: JSON.stringify(userData),
        })
        .then((response) => response.json())
        .then((data) => {
            if (data.status == "success") {
                localStorage.setItem("currentUser", JSON.stringify(data.user))
                location.href = "./main.html"
            } else {
                window.alert('Your Username or Password is incorrect')
            }
        })
        .catch((error) => { console.error("Error sign up", error); })
})
window.addEventListener('keyup', function(event) {
    if (event.keyCode === 13) signIn.click();
})