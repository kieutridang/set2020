const fs = require('fs').promises
const http = require('http')
const url = require('url')
let users = []
const server = http.createServer((request, response) => {
    try {
        let chunks = []
        const resURL = url.parse(request.url, true);
        response.writeHead(200, { 'Access-Control-Allow-Origin': '*' })
        request.on('data', (chunk) => { chunks.push(chunk) })
            .on('end', async() => {
                const crypto = require('crypto')
                if (request.method == 'POST') {
                    if (resURL.pathname == '/sign-up') {
                        const bodyData = JSON.parse(Buffer.concat(chunks).toString())
                        const resData = {}

                        fs.readFile('./users.txt')
                            .then(data => users = (data == '') ? [] : JSON.parse(data))
                            .then(users => {
                                resData.status = 'failed'
                                if (users.every(item => item.username !== bodyData.username)) {
                                    resData.status = 'success'
                                    resData.user = users.length;
                                    bodyData.password = crypto.createHash('sha256').update(bodyData.password).digest('hex')
                                    users.push({ id: users.length, username: bodyData.username, password: bodyData.password })
                                }
                                response.write(JSON.stringify(resData))
                                response.end()
                                return users
                            })
                            .then(users => {
                                fs.writeFile('./users.txt', JSON.stringify(users))
                            }).catch(err => console.error(err))

                    } else if (resURL.pathname == '/sign-in') {
                        const bodyData = JSON.parse(Buffer.concat(chunks).toString())
                        const resData = {}

                        bodyData.password = crypto.createHash('sha256').update(bodyData.password).digest('hex')
                        fs.readFile('./users.txt')
                            .then(data => users = (data == '') ? [] : JSON.parse(data))
                            .then(users => {
                                resData.status = 'failed'
                                if (users.some((item) => item.username == bodyData.username && item.password == bodyData.password)) {
                                    const currentUser = users.find(item => item.username == bodyData.username && item.password == bodyData.password)
                                    resData.status = 'success'
                                    resData.user = currentUser.id;
                                }
                                response.write(JSON.stringify(resData))
                                response.end()
                            })
                            .catch(err => console.error(err))
                    } else if (resURL.pathname == '/change-task') {
                        const bodyData = JSON.parse(Buffer.concat(chunks).toString())
                        fs.writeFile('./tasks.txt', JSON.stringify(bodyData))
                            .catch(err => console.log(err))
                    } else if (resURL.pathname == '/main') {
                        fs.readFile('./tasks.txt')
                            .then(data => {
                                response.write(JSON.stringify(JSON.parse(data)))
                                response.end()
                            })
                            .catch(err => console.log(err))
                    } else if (resURL.pathname == '/signup') {
                        fs.readFile('./users.txt')
                            .then(data => {
                                response.write(JSON.stringify(JSON.parse(data)))
                                response.end()
                            })
                            .catch(err => console.log(err))
                    }
                } else if (request.method == 'GET') {
                    // if (resURL.pathname == '/getTask') {
                    //     fs.readFile('./tasks.txt')
                    //         .then(data => {
                    //             response.write(JSON.stringify(JSON.parse(data)))
                    //             response.end()
                    //         })
                    //         .catch(err => console.log(err))
                    // }
                }
            })
            .on('error', (e) => {
                console.log('Error', e)
                response.end('ERROR')
            })
            .on('timeout', () => {
                console.log('timeout')
                request.abort()
            });
    } catch (err) {
        console.log(err)
        return
    }
});
server.listen(8080, 'localhost', () => {
    console.log('Server started at http://localhost:8080')
});