const fs = require("fs").promises;
const http = require("http");
const url = require("url");
const crypto = require("crypto");

const hostname = "127.0.0.1";
const port = 3000;

const server = http.createServer((request, response) => {
  let chunks = [];

  const url_parts = url.parse(request.url, true);
  const path = url_parts.pathname;
  const query = url_parts.query;

  request
    .on("data", (chunk) => {
      chunks.push(chunk);
    })
    .on("end", async () => {
      switch (path) {
        case "/sign-up":
          if (request.method === "POST") {
            const bodyData = JSON.parse(Buffer.concat(chunks).toString());
            const resData = {};

            fs.readFile("./users.txt", "utf8")
              .then((userList) => {
                const users = JSON.parse(userList);
                if (users.some((user) => user.username === bodyData.username)) {
                  resData.status = "failed";
                } else {
                  bodyData.password = hashString(bodyData.password);
                  bodyData.id = users.length + 1;
                  users.push(bodyData);
                  resData.status = "success";
                  resData.user = bodyData.id;
                  console.log(resData);
                }
                return users;
              })
              .then((users) => {
                return fs.writeFile("./users.txt", JSON.stringify(users));
              })
              .then(() => {
                response.writeHead(200, { "Access-Control-Allow-Origin": "*" });
                response.write(JSON.stringify(resData));
                response.end();
              })
              .catch((error) => {
                console.log(error);
                response.end("ERROR");
              });
          }
          break;
        case "/sign-in":
          if (request.method === "POST") {
            const bodyData = JSON.parse(Buffer.concat(chunks).toString());
            const resData = {};
            bodyData.password = hashString(bodyData.password);

            fs.readFile("./users.txt", "utf8")
              .then((userList) => {
                const users = JSON.parse(userList);
                const user = users.find(
                  (user) =>
                    user.username === bodyData.username &&
                    user.password === bodyData.password
                );
                console.log(users, user);
                if (user) {
                  resData.status = "success";
                  resData.user = user.id;
                } else {
                  resData.status = "failed";
                }
                response.writeHead(200, { "Access-Control-Allow-Origin": "*" });
                response.write(JSON.stringify(resData));
                response.end();
              })
              .catch((error) => {
                console.log(error);
                response.end(error);
              });
          }
          break;
        case "/create-task":
          if (request.method === "POST") {
            const bodyData = JSON.parse(Buffer.concat(chunks).toString());
            const resData = {};

            fs.readFile("./tasks.txt", "utf8")
              .then((taskList) => {
                const allTasks = JSON.parse(taskList);
                return allTasks;
              })
              .then((allTasks) => {
                bodyData.id = allTasks.length + 1;
                allTasks.push(bodyData);
                fs.writeFile("./tasks.txt", JSON.stringify(allTasks));
              })
              .then(() => {
                resData.status = "success";
                response.writeHead(200, { "Access-Control-Allow-Origin": "*" });
                response.write(JSON.stringify(resData));
                response.end();
              })
              .catch((error) => {
                console.log(error);
                response.end("ERROR");
              });
          }
          break;
        case "/list-task":
          if (request.method === "GET") {
            const userId = query.userId;
            const resData = {};

            fs.readFile("./tasks.txt", "utf8")
              .then((taskList) => {
                const allTasks = JSON.parse(taskList);
                const userTasks = allTasks.filter(
                  (task) => task.userId == userId
                );

                resData.status = "success";
                resData.tasks = userTasks;
                response.writeHead(200, { "Access-Control-Allow-Origin": "*" });
                response.write(JSON.stringify(resData));
                response.end();
              })
              .catch((error) => {
                console.log(error);
                response.end("ERROR");
              });
          }
          break;
        default:
          response.end("No url found on this server");
      }
    });
});

const secret = "fuckingsecret";
function hashString(password) {
  const hash = crypto
    .createHmac("sha256", secret)
    .update(password.toString())
    .digest("hex");
  return hash;
}

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});
