const userId = JSON.parse(localStorage.getItem("currentUser")) || "";
const taskField = document.getElementById("tasks");
const inputTaskField = document.getElementById("todo-input");
let userTasks = [];

let store

function init() {
  document.addEventListener('load', () => {
    store = new DataStore()
  })
}

function fetchTasks(userId) {
  fetch(`http://127.0.0.1:3000/list-task?userId=${userId}`)
    .then((response) => response.json())
    .then((data) => {
      if (data.status === "success") {
        userTasks = data.tasks;
        render();
      }
    });
}

function render() {
  console.log(11111)
  console.log(store, 'yes!')
  const statusFilter = document.getElementById("status-filter").value;
  // if (statusFilter === "done") {
  //   userTasks = allTasks.filter(
  //     (task) => task.userId === userId && task.isDone === true
  //   );
  // } else if (statusFilter === "undone") {
  //   userTasks = allTasks.filter(
  //     (task) => task.userId === userId && task.isDone === false
  //   );
  // } else {
  //   userTasks = allTasks.filter((task) => task.userId === userId);
  // }
  taskField.innerHTML = userTasks
    .sort((a, b) => a.id - b.id)
    .map((task) => {
      return `<div key=${task.id}>
                <input type="checkbox" ${
                  task.isDone ? "checked" : ""
                } onchange="main(toggleStatus(${task.id}))"/>
                <input type="text" value=${task.taskName} disabled id="input-${
        task.id
      }"/>
                <button onclick="main(editTask, ${task.id})">edit</button>
                <button onclick="main(deleteTask, ${task.id})">delete</button>
              </div>`;
    })
    .join("");
}

function addTask() {
  let newTask = {
    taskName: inputTaskField.value,
    isDone: false,
    userId: userId,
  };

  fetch("http://127.0.0.1:3000/create-task", {
    method: "POST",
    body: JSON.stringify(newTask),
  })
    .then((response) => response.json())
    .then((data) => {
      if (data.status === "success") {
        console.log("Task created");
      }
    })
    .catch((error) => {
      console.error("Error create task", error);
    });

  inputTaskField.value = "";
  render();
}

// function toggleStatus(taskId) {
//   allTasks = allTasks.map((task) => {
//     if (task.id === taskId) {
//       task.isDone = !task.isDone;
//     }
//     return task;
//   });
//   localStorage.setItem("tasks", JSON.stringify(allTasks));
//   render();
// }

// function editTask(taskId) {
//   let input = document.getElementById(`input-${taskId}`);
//   input.disabled = false;
//   input.addEventListener("keyup", function (event) {
//     // Number 13 is the "Enter" key on the keyboard
//     if (event.keyCode === 13) {
//       // Cancel the default action, if needed
//       event.preventDefault();
//       // Trigger the button element with a click
//       allTasks = allTasks.map((task) => {
//         if (task.id === taskId) {
//           task.taskName = input.value;
//         }
//         return task;
//       });
//       localStorage.setItem("tasks", JSON.stringify(allTasks));
//       render();
//     }
//   });
// }

// function deleteTask(taskId) {
//   debugger
//   allTasks = allTasks.filter((task) => task.id !== taskId);
//   localStorage.setItem("tasks", JSON.stringify(allTasks));
//   render();
// }

function toggleFilter() {
  render();
}

function logout() {
  localStorage.setItem("currentUser", JSON.stringify(""));
  window.location = "./signin.html";
}

function main(fn, params) {
  // debugger
  const userId = JSON.parse(localStorage.getItem("currentUser")) || {};
  if (!userId) {
    window.location = "./signin.html";
    return;
  }
  return fn(params);
}

fetchTasks(userId);
render();

init()