function Tasks() {
  this.tasks = [];
  this.setTasks = function (tasks) {
    this.tasks = tasks;
  };
  this.addTask = function (task) {
    const task = new Task(task);
    this.tasks = this.tasks.push(task);
  };
  this.getTask = function () {
    return this.tasks;
  };
}

function Task(task) {
  this.id = task.id;
  this.taskName = task.taskName;
}
