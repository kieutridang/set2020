const form = document.getElementById("form")
form.addEventListener("submit", submitForm);

function submitForm(e) {
    e.preventDefault()
    const email = document.getElementById("email-field")
    const password = document.getElementById("password-field")
    let emailErrorField = document.getElementById("email-errors")
    let passwordErrorField = document.getElementById("password-errors")

    const bodyData = {
        username: email.value,
        password: password.value,
      };
    fetch("http://127.0.0.1:3000/sign-in", {
      method: "POST",
      body: JSON.stringify(bodyData),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "success") {
            localStorage.setItem("currentUser", JSON.stringify(data.user))
            store.currentUser.setUser(data.user, email.value)
            window.location = "./main.html"
        } else {
            emailErrorField.innerHTML = `<div>Your email or password is incorrect</div>`
        }
      })
      .catch((error) => {
        console.error("Error sign up", error);
        emailErrorField.innerHTML = `<div>${error}</div>`
      });
}

function checkCurrentUser() {
    const currentUser = JSON.parse(localStorage.getItem("currentUser")) || {}
    const userId = currentUser.id
    if (userId) {
      window.location = "./main.html"
    }
}
checkCurrentUser()