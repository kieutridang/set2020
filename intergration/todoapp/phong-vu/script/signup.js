const form = document.getElementById("form");
form.addEventListener("submit", submitForm);

function submitForm(e) {
  e.preventDefault();
  const email = document.getElementById("email-field");
  const password = document.getElementById("password-field");
  const emailValidation = validateEmail(email.value);
  const passwordValidation = validatePassword(password.value);
  let emailErrorField = document.getElementById("email-errors");
  let passwordErrorField = document.getElementById("password-errors");

  if (!emailValidation.isValid) {
    emailErrorField.innerHTML = emailValidation.errors
      .map((error) => {
        return `<div>${error}</div>`;
      })
      .join("");
    email.className = "error";
  } else {
    emailErrorField.innerHTML = null;
  }
  if (!passwordValidation.isValid) {
    passwordErrorField.innerHTML = passwordValidation.errors
      .map((error) => {
        return `<div>${error}</div>`;
      })
      .join("");
    password.className = "error";
  } else {
    passwordErrorField.innerHTML = null;
  }
  if (emailValidation.isValid && passwordValidation.isValid) {
    const bodyData = {
      username: email.value,
      password: password.value,
    };
    fetch("http://127.0.0.1:3000/sign-up", {
      method: "POST",
      body: JSON.stringify(bodyData),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data.status === "success") {
            localStorage.setItem("currentUser", JSON.stringify(data.user))
            window.location = "./main.html"
        }
      })
      .catch((error) => {
        console.error("Error sign up", error);
      });
  }
}

function validateEmail(email) {
  let result = {
    isValid: true,
    errors: [],
  };
  // const domainCheck = /.*\@[a-zA-Z]{3,}\./
  // if (!domainCheck.test(email)) {
  //     result.errors.push('Must must have domain')
  // }
  // const aCheck = /.*\@.*/
  // if (!aCheck.test(email)) {
  //     result.errors.push('Must have "@"')
  // }
  // const formatCheck = /.+@.+\..+/
  // if (!formatCheck.test(email)) {
  //     result.errors.push('Wrong format')
  // }
  // const upperCaseCheck  = /[A-Z]/
  // if (upperCaseCheck.test(email)) {
  //     result.errors.push('Cannot contain uppercase characters')
  // }
  // if (result.errors.length) {
  //     result.isValid = false
  // }
  return result;
}

function validatePassword(password) {
  let result = {
    isValid: true,
    errors: [],
  };
  // const lengthCheck = /^.{8,24}$/
  // if (!lengthCheck.test(password)) {
  //     console.log(!lengthCheck.test(password))
  //     result.errors.push('Must from 8-24 characters')
  // }
//   const upperCaseCheck = /[A-Z]/;
//   if (!upperCaseCheck.test(password)) {
//     result.errors.push("Must have upperCase characters");
//   }
//   const lowerCaseCheck = /[a-z]/;
//   if (!lowerCaseCheck.test(password)) {
//     result.errors.push("Must have lowwerCase characters");
//   }
//   const numberCheck = /[0-9]/;
//   if (!numberCheck.test(password)) {
//     result.errors.push("Must have numbers");
//   }
//   const specialCharacterCheck = /[^A-Za-z0-9]/;
//   if (!specialCharacterCheck.test(password)) {
//     result.errors.push("Must have special characters");
//   }
  // const spaceCheck = /[\s]/
  // if (spaceCheck.test(password)) {
  //     result.errors.push('Cannot contain blank space')
  // }
  // const lastUpperCaseCheck = /[A-Z]$/
  // if (lastUpperCaseCheck.test(password)) {
  //     result.errors.push('Last character cannot be uppercase')
  // }
  const badWordCheck = /(?=.*fuck)|(?=.*dick)|(?=.*cunt)|(?=.*shit)|(?=.*bitch)/;
  if (badWordCheck.test(password)) {
    result.errors.push("Wanna fuck me?");
  }
  if (result.errors.length) {
    result.isValid = false;
  }
  return result;
}

function checkCurrentUser() {
  const currentUser = JSON.parse(localStorage.getItem("currentUser")) || {};
  const userId = currentUser.id;
  if (userId) {
    window.location = "./main.html";
  }
}
checkCurrentUser();
