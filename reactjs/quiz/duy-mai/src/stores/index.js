import React from 'react'

export { default as reducer } from './reducer'

export const StoreContext = React.createContext();

export const initialState = {
  user: {
    username: ''
  }
}

export default function useStore() {
  return React.useContext(StoreContext)
}