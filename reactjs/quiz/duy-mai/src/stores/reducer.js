export default function reducer(state, action) {
  switch (action.type) {
    case 'update-username':
      return {
        ...state,
        user: {
          ...state.user,
          username: action.payload
        }
      }

    default:
      return state
  }
}
