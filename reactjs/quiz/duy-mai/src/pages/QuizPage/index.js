import React from "react";
import QuestionBox from "../../components/QuestionBox";
import { questions } from "./constants";

function QuizPage(props) {
  // const [answers, setAnswers]=useState(Array(questions.length))
  const answers = Array(questions.length);

  function handleSelectAnswer(index, answer) {
    answers[index] = answer;
  }

  function handleSubmit() {
    console.log("answers", answers);
    const score = questions.reduce((totalScore, question, index) => {
      return answers[index] === question.correctChoice
        ? totalScore + question.score
        : totalScore;
    }, 0);
    alert(`Your score is: ${score || 0}`);
  }

  return (
    <div>
      <div>
        {questions.map((question, index) => (
          <QuestionBox
            key={Math.random().toString()}
            {...question}
            index={index}
            onAnswerSelect={(selectedAnswer) =>
              handleSelectAnswer(index, selectedAnswer)
            }
          />
        ))}
      </div>
      <button onClick={handleSubmit}>Submit</button>
    </div>
  );
}

export default QuizPage;
