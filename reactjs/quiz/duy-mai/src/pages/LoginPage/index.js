import React from "react";
import { Link } from 'react-router-dom';
import useStore from "../../stores";

function LoginPage(props) {
  const { state, dispatch } = useStore()

  function handleChangeUsername(e) {
    dispatch({ type: 'update-username', payload: e.target.value })
  }

  return (
    <div>
      <p>{state.user.username}</p>
      <input placeholder="Your username" onChange={handleChangeUsername} />
      <Link to='/quiz'>
        <button>Start quiz</button>
      </Link>
    </div>
  )
}

export default LoginPage;
