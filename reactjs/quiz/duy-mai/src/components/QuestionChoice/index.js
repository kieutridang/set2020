import React from "react";
import './questionBox.css'

function QuestionChoice(props) {
  const {
    questionId = Math.random().toString(),
    choice,
    onAnswerSelect,
  } = props;

  return (
    <label className="choice-label" onClick={() => onAnswerSelect(choice)}>
      <input
        type="radio"
        name={questionId}
        value={`${questionId}-${choice}`}
      />
      {choice}
    </label>
  );
}

export default QuestionChoice;
