import React from "react";
import QuestionChoice from '../QuestionChoice';
import './questionBox.css';

function QuestionBox(props) {
  const {
    id = Math.random().toString(),
    index,
    question = "",
    choices = [],
    onAnswerSelect,
  } = props;

  return (
    <div>
      <p>{index + 1}. {question}</p>
      <div className="choice-wrapper">
        {choices.map((choice) => (
          <QuestionChoice key={Math.random().toString()} questionId={id} choice={choice} onAnswerSelect={onAnswerSelect} />
        ))}
      </div>
    </div>
  );
}

export default QuestionBox;
