var id = 0

function Employee(name, department) {
  this.id = id++
  this.name = name
  this.department = department
}

function Manager(name, department) {
  this.reports = []
  this.test = function () {
    console.log('test')
  }
  Employee.call(this, name, department)
  // Manager.prototype = Object.create(Employee.prototype)
  // Manager.prototype.constructor = Manager
  this.test1 = function () {
    console.log('test1')
  }
}

Manager.prototype.test2 = function test2 () {
  console.log('test2')
}

const quan = new Employee('Quan', 'marketing')
const duy = new Manager('Duy', 'marketing')
debugger
